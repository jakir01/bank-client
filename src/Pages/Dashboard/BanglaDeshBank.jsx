import { useState, useEffect } from "react";
import axios from "axios";
import { FaUserTie, FaCheckCircle, FaList, FaUser } from "react-icons/fa"; // Importing icons
import { FaBuildingColumns } from "react-icons/fa6";

const BanglaDeshBank = () => {
  const [newBankName, setNewBankName] = useState("");
  const [newBankEmail, setNewBankEmail] = useState("");
  const [newBankPassword, setNewBankPassword] = useState("");
  const [isCreatingBank, setIsCreatingBank] = useState(false);
  const [showAgentList, setShowAgentList] = useState(false);
  const [showVerifyList, setShowVerifyList] = useState(false);
  const [showBankList, setShowBankList] = useState(false);
  const [branches, setBranches] = useState([]);
  const [banks, setBanks] = useState([]);
  const [bankIdCounter, setBankIdCounter] = useState(0);
  const [errors, setErrors] = useState({});
  const [verifyList, setVerifyList] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const [activeTab, setActiveTab] = useState("");
  const [showUserForm, setShowUserForm] = useState(false);
  const [formData, setFormData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:5000/bank/lastId")
      .then((response) => setBankIdCounter(response.data.lastId + 1))
      .catch((error) => console.error("Error fetching last bank ID:", error));

    axios
      .get("http://localhost:5000/branches")
      .then((response) => setBranches(response.data))
      .catch((error) => console.error("Error fetching branches:", error));

    axios
      .get("http://localhost:5000/bank")
      .then((response) => setBanks(response.data))
      .catch((error) => console.error("Error fetching banks:", error));

    axios
      .get("http://localhost:5000/submits")
      .then((response) => {
        // No need to filter as the backend already provides the filtered data.
        setVerifyList(response.data);
      })
      .catch((error) =>
        console.error("Error fetching verification list:", error)
      );
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:5000/submits/");
      if (!response.ok) {
        throw new Error("Failed to fetch data");
      }
      const data = await response.json();
      // Filter data where payment is "Paid"
      const filtered = data.filter((item) => item.payment === "Paid");
      setFilteredData(filtered);

      // Automatically handle accept for filtered items
      filtered.forEach((item) => {
        handleAccept(item);
      });
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // useEffect to fetch data when component mounts
  useEffect(() => {
    fetchData();
  }, []);

  const handleAccept = (item) => {
    const userAccountId = generateUniqueId(
      item.formId,
      item.branchId,
      item.bankId,
      item.bdBankId
    );
    const updatedItem = { ...item, userAccountId, status: "Verified" };
    updateVerificationStatus(updatedItem);
    createVerifiedAccount(updatedItem);
    closeModal();
  };

  const handleDecline = (item) => {
    const updatedItem = { ...item, status: "bank verify" };
    updateVerificationStatus(updatedItem);
    closeModal();
  };

  // const updateVerificationStatus = (updatedItem) => {
  //   axios
  //     .patch(`http://localhost:5000/submits/${updatedItem._id}`, {
  //       status: updatedItem.status,
  //     })
  //     .then((response) => {
  //       console.log("Verification request updated:", response.data);
  //       setVerifyList((prevList) =>
  //         prevList.map((el) => (el.id === updatedItem.id ? updatedItem : el))
  //       );
  //     })
  //     .catch((error) =>
  //       console.error("Error updating verification request:", error)
  //     );
  // };
  const updateVerificationStatus = (updatedItem) => {
    axios
      .patch(`http://localhost:5000/submits/${updatedItem._id}`, {
        status: updatedItem.status,
        payment: updatedItem.payment, // new field
        amount: updatedItem.amount, // new field
      })
      .then((response) => {
        console.log("Verification request updated:", response.data);
        setVerifyList((prevList) =>
          prevList.map((el) => (el._id === updatedItem._id ? updatedItem : el))
        );
      })
      .catch((error) =>
        console.error("Error updating verification request:", error)
      );
  };
  

  const createVerifiedAccount = (verifiedData) => {
    const { formId, bid, bdBankId, bankVerificationId, ...rest } = verifiedData;
    const userAccountId = generateUniqueId(
      bid,
      bankVerificationId,
      bdBankId,
      formId
    );
    const verifiedAccountData = { userAccountId, ...rest };

    axios
      .post("http://localhost:5000/verifiedAccount", verifiedAccountData)
      .then((response) =>
        console.log("Verified account created:", response.data)
      )
      .catch((error) =>
        console.error("Error creating verified account:", error)
      );
  };

  // const generateUniqueId = ( branchId) => {
  //   const bdBankId = Math.floor(Math.random() * 10) + 1;
  //   const uniqueId = `${branchId}-${bdBankId}-${Math.floor(Math.random() * 10)}`;
  //   return uniqueId;
  // };

  const generateUniqueId = (formId, bid, bankVerificationId) => {
    // const formattedBranchId = String(branchId).padStart(4, '0');
    const formattedBdBankId = String(
      Math.floor(Math.random() * 100000000)
    ).padStart(4, "0");
    // const formattedId = String(id).padStart(4, '0');
    const uniqueId = `${formId}-${bid}-${bankVerificationId}-${formattedBdBankId}`;
    return uniqueId;
  };

  const validateForm = () => {
    let tempErrors = {};
    let isValid = true;

    if (!newBankName.trim()) {
      tempErrors.name = "Bank name is required";
      isValid = false;
    }
    if (!newBankEmail.trim()) {
      tempErrors.email = "Email is required";
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(newBankEmail)) {
      tempErrors.email = "Email is invalid";
      isValid = false;
    }
    if (!newBankPassword.trim()) {
      tempErrors.password = "Password is required";
      isValid = false;
    } else if (newBankPassword.length < 6) {
      tempErrors.password = "Password must be at least 6 characters long";
      isValid = false;
    }

    setErrors(tempErrors);
    return isValid;
  };

  const handleCreateBank = () => {
    if (!validateForm()) {
      return;
    }

    const randomNumber = Math.floor(Math.random() * 10000000); // Generate random number between 0 and 9999999
    const bankId = `b${randomNumber.toString().padStart(7, "0")}`; // Pad the random number with zeros to ensure 7 digits

    const newBank = {
      bankId,
      bankName: newBankName,
      email: newBankEmail,
      password: newBankPassword,
      role: "bank",
    };

    console.log("New Bank Created:", newBank);
    setNewBankName("");
    setNewBankEmail("");
    setNewBankPassword("");
    setErrors({});

    axios
      .post("http://localhost:5000/bank", newBank)
      .then((response) =>
        console.log("Bank creation successful:", response.data)
      )
      .catch((error) => console.error("Error creating bank:", error));
  };

  const handleStatusUpdate = (branchId, newStatus) => {
    axios
      .patch(`http://localhost:5000/branches/${branchId}`, {
        status: newStatus,
      })
      .then((response) => {
        console.log("Status updated successfully:", response.data);
        // Update branches state to reflect the changes
        setBranches((prevBranches) => {
          return prevBranches.map((branch) => {
            if (branch.branchId === branchId) {
              return { ...branch, status: newStatus };
            }
            return branch;
          });
        });
      })
      .catch((error) => console.error("Error updating status:", error));
  };

  const handleOpenForm = (item) => {
    setSelectedItem(item);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setSelectedItem(null);
  };

  useEffect(() => {
    setIsCreatingBank(true);
    setShowAgentList(false);
    setShowVerifyList(false);
    setShowBankList(false);
  }, []);

  return (
    <div className="flex  min-h-screen bg-gray-100">
      <div className="bg-gray-800 text-white w-1/4 p-6 space-y-6">
      <h1 className="text-2xl font-bold mb-5 bg-gradient-to-r from-indigo-500 via-green-500 to-pink-500 bg-clip-text text-transparent">
        Bangladesh Regulatory Authority
      </h1>
      <ul>
        <li
          className={`flex items-center p-2 mt-2 cursor-pointer text-indigo-200 hover:bg-sky-600 ${
            activeTab === "createBank" && "bg-sky-600"
          }`}
          onClick={() => {
            setIsCreatingBank(true);
            setShowAgentList(false);
            setShowVerifyList(false);
            setShowBankList(false);
            setShowUserForm(false);
            setActiveTab("createBank");
          }}
        >
          <FaBuildingColumns  className="inline-block mr-2" /> {/* Icon */}
          <span>Create Insurance Company</span>
        </li>
        <li
          className={`flex items-center p-2 mt-2 cursor-pointer text-indigo-200 hover:bg-sky-600 ${
            activeTab === "branchList" && "bg-sky-600"
          }`}
          onClick={() => {
            setShowAgentList(true);
            setIsCreatingBank(false);
            setShowVerifyList(false);
            setShowBankList(false);
            setShowUserForm(false);
            setActiveTab("branchList");
          }}
        >
          <FaUserTie className="inline-block mr-2" /> {/* Icon */}
          <span>Agent List</span>
        </li>
        <li
          className={`flex items-center p-2 mt-2 cursor-pointer text-indigo-200 hover:bg-sky-600 ${
            activeTab === "verifyList" && "bg-sky-600"
          }`}
          onClick={() => {
            setShowVerifyList(true);
            setIsCreatingBank(false);
            setShowAgentList(false);
            setShowBankList(false);
            setShowUserForm(false);
            setActiveTab("verifyList");
          }}
        >
          <FaCheckCircle className="inline-block mr-2" /> {/* Icon */}
          <span>Verify List</span>
        </li>
        <li
          className={`flex items-center p-2 mt-2 cursor-pointer text-indigo-200 hover:bg-sky-600 ${
            activeTab === "insuranceList" && "bg-sky-600"
          }`}
          onClick={() => {
            setShowBankList(true);
            setIsCreatingBank(false);
            setShowAgentList(false);
            setShowVerifyList(false);
            setShowUserForm(false);
            setActiveTab("insuranceList");
          }}
        >
          <FaList className="inline-block mr-2" /> {/* Icon */}
          <span>Insurance Company List</span>
        </li>
        <li
          className={`flex items-center p-2 mt-2  cursor-pointer text-indigo-200 hover:bg-sky-600 ${
            activeTab === "userForm" && "bg-sky-600"
          }`}
          onClick={() => {
            setShowUserForm(true);
            setIsCreatingBank(false);
            setShowBankList(false);
            setShowAgentList(false);
            setShowVerifyList(false);
            setActiveTab("userForm");
          }}
        >
          <FaUser className="inline-block mr-2" /> {/* Icon */}
          <span>User Form</span>
        </li>
      </ul>
    </div>

      <div className="flex-1 p-10">
        {isCreatingBank && (
          <div className="max-w-lg mx-auto bg-white p-8 border border-gray-200 rounded-lg shadow-sm">
            <h2 className="text-xl font-semibold mb-6 bg-gradient-to-r from-indigo-500 via-green-500 to-pink-500  bg-clip-text text-transparent">
              Create New Insurance Co.
            </h2>
            <div className="space-y-4">
              <input
                type="text"
                placeholder="Insurance Company Name"
                value={newBankName}
                onChange={(e) => setNewBankName(e.target.value)}
                className={`mt-1 block w-full px-3 py-2 bg-white border ${
                  errors.name ? "border-red-500" : "border-gray-300"
                } rounded-md text-sm shadow-sm placeholder-gray-400 focus:outline-none focus:border-indigo-500`}
              />
              {errors.name && (
                <p className="text-red-500 text-xs italic">{errors.name}</p>
              )}
              <input
                type="email"
                placeholder="Email"
                value={newBankEmail}
                onChange={(e) => setNewBankEmail(e.target.value)}
                className={`mt-1 block w-full px-3 py-2 bg-white border ${
                  errors.email ? "border-red-500" : "border-gray-300"
                } rounded-md text-sm shadow-sm placeholder-gray-400 focus:outline-none focus:border-indigo-500`}
              />
              {errors.email && (
                <p className="text-red-500 text-xs italic">{errors.email}</p>
              )}
              <input
                type="password"
                placeholder="Password"
                value={newBankPassword}
                onChange={(e) => setNewBankPassword(e.target.value)}
                className={`mt-1 block w-full px-3 py-2 bg-white border ${
                  errors.password ? "border-red-500" : "border-gray-300"
                } rounded-md text-sm shadow-sm placeholder-gray-400 focus:outline-none focus:border-indigo-500`}
              />
              {errors.password && (
                <p className="text-red-500 text-xs italic">{errors.password}</p>
              )}
              <button
                onClick={handleCreateBank}
                className="w-full text-white px-4 py-2 rounded-md focus:outline-noneZ
                bg-gradient-to-r from-blue-500 to-green-500 hover:from-pink-500 hover:to-yellow-500
                "
              >
                Create Insurance Company
              </button>
            </div>
          </div>
        )}

        {showAgentList && (
          <div className="max-w-2xl mx-auto bg-white p-8 border border-gray-200 rounded-lg shadow-sm">
            <h2 className="text-xl font-semibold mb-6">Agent List</h2>
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Agent ID
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Bank ID
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Insurance Company
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Branch Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Status
                  </th>
                </tr>
              </thead>
              <tbody>
                {branches.map((branch) => (
                  <tr key={branch.branchId}>
                    <td
                      className={`px-5 py-5 border-b border-gray-200 bg-white text-sm ${
                        branch.status === "inactive" ? "text-red-500" : ""
                      }`}
                    >
                      {branch.branchId}
                    </td>
                    <td
                      className={`px-5 py-5 border-b border-gray-200 bg-white text-sm ${
                        branch.status === "inactive" ? "text-red-500" : ""
                      }`}
                    >
                      {branch.bankId}
                    </td>
                    <td
                      className={`px-5 py-5 border-b border-gray-200 bg-white text-sm ${
                        branch.status === "inactive" ? "text-red-500" : ""
                      }`}
                    >
                      {branch.bankName}
                    </td>
                    <td
                      className={`px-5 py-5 border-b border-gray-200 bg-white text-sm ${
                        branch.status === "inactive" ? "text-red-500" : ""
                      }`}
                    >
                      {branch.branchName}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <button
                        className={`px-4 py-2 rounded-full ${
                          branch.status === "active"
                            ? "bg-green-500 hover:bg-green-700 text-white"
                            : "bg-red-600 hover:bg-red-700 text-white"
                        }`}
                        onClick={() =>
                          handleStatusUpdate(
                            branch.branchId,
                            branch.status === "active" ? "inactive" : "active"
                          )
                        }
                      >
                        {branch.status === "active" ? "Active" : "Inactive"}
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}

        {showVerifyList && (
          <div className="max-w-4xl mx-auto bg-white p-8 border border-gray-200 rounded-lg shadow-sm">
            <h2 className="text-xl font-semibold mb-6">Verification List</h2>
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Agent ID
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    User Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Father's Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Mother's Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    User Email
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Insurance Company
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Branch Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Status
                  </th>
                </tr>
              </thead>
              <tbody>
                {verifyList
                  .filter((item) => item.status === "Bank Verify")
                  .map((item) => (
                    <tr key={item.id}>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.branchId}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.userName}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.fatherName}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.motherName}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.email}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.selectedBank}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.bankAgentName}
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {item.status === "Bank Verify" && (
                          // <>
                          //   <button className="bg-green-500 text-white px-2 py-1 rounded-md mr-2 mb-1" onClick={() => handleAccept(item)}>
                          //     Accept
                          //   </button>
                          //   <button className="bg-red-500 text-white px-2 py-1 rounded-md" onClick={() => handleDecline(item)}>
                          //     Decline
                          //   </button>
                          // </>
                          <button
                            onClick={() => handleOpenForm(item)}
                            className="mt-4 py-2 px-4 bg-gradient-to-r from-blue-500 to-green-500 hover:from-pink-500 hover:to-yellow-500 text-white rounded-md"
                          >
                            View
                          </button>
                        )}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        )}
        {isModalOpen && selectedItem && (
          <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
            <div className="bg-white p-6 rounded-md shadow-lg w-96 md:w-[32rem]">
              <h2 className="text-xl font-semibold mb-4">
                Verification Details
              </h2>
              <div className="grid grid-cols-2 gap-4">
                <p className="font-semibold">Agent ID :</p>
                <p>{selectedItem.branchId}</p>
                <p className="font-semibold">User Name :</p>
                <p>{selectedItem.userName}</p>
                <p className="font-semibold">Father's Name :</p>
                <p>{selectedItem.fatherName}</p>
                <p className="font-semibold">Mother's Name :</p>
                <p>{selectedItem.motherName}</p>
                <p className="font-semibold">User Email :</p>
                <p>{selectedItem.email}</p>
                <p className="font-semibold">Bank Name :</p>
                <p>{selectedItem.selectedBank}</p>
                <p className="font-semibold">Agent Name :</p>
                <p>{selectedItem.bankAgentName}</p>
                <p className="font-semibold">Address :</p>
                <p>{selectedItem.address}</p>
                <p className="font-semibold">Insurance Type :</p>
                <p>{selectedItem.insuranceType}</p>
                <p className="font-semibold">Deposit Amount :</p>
                <p>{selectedItem.amount}</p>
                <p className="font-semibold">Payment :</p>
                <p>{selectedItem.payment}</p>
                <p className="font-semibold">Duration :</p>
                <p>{selectedItem.duration}</p>
                {/* Add more fields as needed */}
              </div>
              <div className="mt-6 flex space-x-4 btn-sm text-sm">
                {/* <button
                  onClick={() => handleAccept(selectedItem)}
                  className="py-2 px-4 bg-green-600 hover:bg-green-500 text-white rounded-md"
                >
                  Accept
                </button>
                <button
                  onClick={() => handleDecline(selectedItem)}
                  className="py-2 px-4 bg-red-600 hover:bg-red-400 text-white rounded-md"
                >
                  Decline
                </button> */}
                <button
                  onClick={closeModal}
                  className="py-2 px-6 bg-gray-600 hover:bg-gray-500  text-white rounded-md"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        )}

        {showBankList && (
          <div className="max-w-2xl mx-auto bg-white p-8 border border-gray-200 rounded-lg shadow-sm">
            <h2 className="text-xl font-semibold mb-6">
              Insurance Company List
            </h2>
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    ID
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Insurance Company
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-left text-xs font-semibold text-gray-200 uppercase tracking-wider">
                    Email
                  </th>
                </tr>
              </thead>
              <tbody>
                {banks.map((bank) => (
                  <tr key={bank.bankId}>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {bank.bankId}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {bank.bankName}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {bank.email}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
        {showUserForm && (
          <div className="max-w-3xl mx-auto bg-white p-8 border border-gray-200 rounded-lg shadow-sm">
            <h2 className="text-xl font-semibold mb-6">All Forms</h2>
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold uppercase tracking-wider">
                    Sl. NO
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    formId
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold  uppercase tracking-wider">
                    Bank Id
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold  uppercase tracking-wider">
                    Agent ID
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold  uppercase tracking-wider">
                    Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold  uppercase tracking-wider">
                    Payment
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-blue-400 text-gray-200 text-left text-xs font-semibold  uppercase tracking-wider">
                    Action
                  </th>
                  {/* Add other table headers based on API response */}
                </tr>
              </thead>
              <tbody>
                {filteredData.map((item, index) => (
                  <tr key={index}>
                    <td className="py-3 px-6 text-left">{index + 1}</td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {item.formId}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {item.bankId}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {item.branchId}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {item.userName}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {item.payment}
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {
                        <button
                          onClick={() => handleOpenForm(item)}
                          className="mt-4 py-2 px-4 bg-gradient-to-r from-blue-500 to-green-500 hover:from-pink-500 hover:to-yellow-500 text-white rounded-md"
                        >
                          View
                        </button>
                      }
                    </td>
                    {/* Add other table cells based on API response */}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    </div>
  );
};

export default BanglaDeshBank;
