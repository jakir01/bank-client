import { useState, useEffect } from "react";
import axios from "axios";

const BangladeshBankDashboard = () => {
  const [bankData, setBankData] = useState([]);
  const [selectedAgent, setSelectedAgent] = useState(null);
  const [verifiedUsers, setVerifiedUsers] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("http://localhost:5000/submits");
        setBankData(response.data);
      } catch (error) {
        console.error("Error fetching bank data:", error);
      }
    };

    fetchData();
  }, []);

  const renderAgents = () => {
    const agents = bankData
      .filter(
        (data) => data.status === "approved" || data.status === "verified"
      )
      .map((data) => data.bankAgentName);

    const uniqueAgents = [...new Set(agents)];
    return uniqueAgents.map((agent, index) => (
      <li
        key={index}
        className="cursor-pointer"
        onClick={() => handleAgentClick(agent)}
      >
        {agent}
      </li>
    ));
  };

  const renderSubmissions = () => {
    if (!selectedAgent) return null;
    const submissions = bankData
      .filter(
        (data) =>
          data.bankAgentName === selectedAgent &&
          (data.status === "approved" || data.status === "verified")
      )
      .slice() // Make a shallow copy of the array before reversing
      .reverse(); // Reverse the copy for display
    return submissions.map((data, index) => (
      <div
        key={index}
        className="w-full sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 px-4 mb-8"
      >
        <div className="bg-gray-100 rounded-lg p-4">
          <div className="mb-4">
            <p>User Name: {data.userName}</p>
            <p>Father's Name: {data.fatherName}</p>
            <p>Mother's Name: {data.motherName}</p>
            <p>Date of Birth: {data.dateOfBirth}</p>
            <p>NID: {data.nidNumber}</p>
            <p>Bank: {data.selectedBank}</p>
            <p>Bank Agent Name: {data.bankAgentName}</p>
            <p>Bank Agent Id: {data.agentId}</p>
            <p>Bangladesh Bank Id: {data.bdBankId}</p>
            <p>User Account Id: {data.userAccountId}</p>
          </div>
          <div className="mb-4">
            <p>
              Bank Agent Status:{" "}
              <span
                className={
                  data.bankAgentStatus === "active"
                    ? "text-green-500 font-bold"
                    : "text-red-500 font-bold"
                }
              >
                {data.bankAgentStatus}
              </span>
            </p>
          </div>
          <div className="mb-4">{renderAgentControls(data)}</div>
        </div>
      </div>
    ));
  };

  const renderAgentControls = (data) => {
    if (data.status === "verified") {
      return null; // Hide buttons for verified accounts
    } else if (data.bankAgentStatus === "active") {
      return (
        <div className="mt-4">
          <button
            onClick={() => handleStatusChange(data._id, "verified")}
            className="bg-blue-500 text-white px-2 py-1 rounded mr-2"
          >
            Verify
          </button>
          <button
            onClick={() => toggleAgentStatus(data._id, "inactive")}
            className="bg-red-500 text-white px-2 py-1 rounded"
          >
            Inactivate
          </button>
        </div>
      );
    } else {
      return (
        <div className="mt-4">
          <button
            onClick={() => toggleAgentStatus(data._id, "active")}
            className="bg-green-500 text-white px-2 py-1 rounded mr-2"
          >
            Activate
          </button>
          <button
            onClick={() => toggleAgentStatus(data._id, "inactive")}
            className="bg-red-500 text-white px-2 py-1 rounded"
          >
            Inactivate
          </button>
        </div>
      );
    }
  };

  const handleAgentClick = (agentName) => {
    setSelectedAgent(agentName);
  };

  const handleStatusChange = async (id, newStatus) => {
    try {
      const bdBankId = generateBdBankId();
      const userAccountId = generateUserAccountId(
        bankData.find((data) => data._id === id),
        bdBankId // Pass bdBankId here
      );
      await axios.patch(`http://localhost:5000/submits/${id}`, {
        status: newStatus,
        bdBankId: bdBankId,
        userAccountId: userAccountId,
      });
      const updatedBankData = bankData.map((data) => {
        if (data._id === id) {
          return {
            ...data,
            status: newStatus,
            bdBankId: bdBankId,
            userAccountId: userAccountId,
          };
        }
        return data;
      });
      setBankData(updatedBankData);
      updateVerifiedUsers(updatedBankData); // Update verified users list
      await saveVerifiedAccountToBackend(
        updatedBankData.find((data) => data._id === id)
      ); // Save verified account to backend
    } catch (error) {
      console.error("Error updating status:", error);
    }
  };

  const toggleAgentStatus = async (id, newStatus) => {
    try {
      const bdBankId = generateBdBankId();
      await axios.patch(`http://localhost:5000/submits/${id}`, {
        bankAgentStatus: newStatus,
        bdBankId: bdBankId,
      });
      const updatedBankData = bankData.map((data) => {
        if (data._id === id) {
          return { ...data, bankAgentStatus: newStatus, bdBankId: bdBankId };
        }
        return data;
      });
      setBankData(updatedBankData);
      updateVerifiedUsers(updatedBankData); // Update verified users list
    } catch (error) {
      console.error("Error updating status:", error);
    }
  };

  const generateBdBankId = () => {
    return Math.floor(Math.random() * 1000000);
  };

  // const generateFdrAccountId = () => {
  //   // Implement your logic to generate fdrAccountId
  //   // This is just a placeholder
  //   return "fdrAccountId";
  // };

  const generateUserAccountId = (data, bdBankId) => {
    // Generate user account ID by combining form ID, agent ID, and Bangladesh Bank ID
    return `${data._id}-${data.agentId}-${bdBankId}`;
  };

  const updateVerifiedUsers = (bankData) => {
    // Filter out verified users
    const verified = bankData.filter((data) => data.status === "verified");
    setVerifiedUsers(verified);
  };

  const saveVerifiedAccountToBackend = async (verifiedAccount) => {
    try {
      // Send verified account data to backend endpoint
      await axios.post(
        "http://localhost:5000/verifiedAccount",
        verifiedAccount
      );
    } catch (error) {
      console.error("Error saving verified account to backend:", error);
    }
  };

  // Other functions...

  return (
    <div className="flex h-screen">
      <div className="bg-teal-950 text-white w-64 flex-none">
        <div className="p-4">
          <h1 className="text-2xl font-semibold">Bangladesh Bank</h1>
          <p className="mt-4"> Bank Agents List</p>
          <ul className="mt-4">{renderAgents()}</ul>
        </div>
      </div>

      <div className="flex-1 p-8">
        <h1 className="text-3xl font-semibold mb-8">
          Bangladesh Bank Dashboard
        </h1>
        {/* <h2 className="text-xl font-semibold mb-4">Verified Users</h2> */}
        {selectedAgent && (
          <h2 className="text-lg font-semibold text-teal-700 mb-4">
            {bankData.length > 0 &&
              `${
                bankData.find((data) => data.bankAgentName === selectedAgent)
                  ?.selectedBank
              }`}{" "}
            <br />
            Bank Agent: {selectedAgent}
          </h2>
        )}
        <div className="flex flex-wrap -mx-4">{renderSubmissions()}</div>
      </div>
    </div>
  );
};

export default BangladeshBankDashboard;
