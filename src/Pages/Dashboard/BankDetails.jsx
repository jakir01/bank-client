import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

const BankDetails = ({ id, branchName }) => {
  const [accounts, setAccounts] = useState([]);
  const { branchId } = useParams();
  const [selectedAccount, setSelectedAccount] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);

  // Function to fetch accounts for a specific branch
  const fetchAccountsForBranch = () => {
    axios
      .get(`http://localhost:5000/submits?branchId=${branchId}`)
      .then((response) => {
        setAccounts(response.data);
      })
      .catch((error) => {
        console.error("Error fetching accounts:", error);
      });
  };

  useEffect(() => {
    if (branchId) {
      fetchAccountsForBranch(branchId); // Fetch accounts for the current branchId
    }
  }, [branchId]);

  // Function to handle opening the modal and setting selected account
  const openModal = (account) => {
    setSelectedAccount(account);
    setModalOpen(true);
  };

  // Function to handle closing the modal
  const closeModal = () => {
    setModalOpen(false);
    setSelectedAccount(null); // Reset selectedAccount state
  };

  // Function to handle status change
  const handleStatusChange = (accountId, status, bid, bankVerificationId) => {
    // Send updated data to backend
    axios
      .patch(`http://localhost:5000/submits/${accountId}`, {
        status: status,
        bid: bid,
        bankVerificationId: bankVerificationId,
      })
      .then((response) => {
        console.log("Status updated successfully:", response.data);

        // Update accounts state to reflect the updated status
        setAccounts(
          accounts.map((account) =>
            account._id === accountId
              ? {
                  ...account,
                  status: status,
                  bid: bid,
                  bankVerificationId: bankVerificationId,
                }
              : account
          )
        );

        // Close the modal after status update
        setModalOpen(false);
        setSelectedAccount(null); // Reset selectedAccount state
      })
      .catch((error) => {
        console.error("Error updating status:", error);
      });
  };

  // Function to generate a unique 8-digit bankVerificationId
  const generateBankVerificationId = () => {
    const id = Math.floor(10000000 + Math.random() * 90000000); // Generate a random number between 10000000 and 99999999
    return id.toString(); // Convert to string
  };

  return (
    <div className="container mx-auto py-6">
      <table className="min-w-full divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              S. No
            </th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              Branch Id
            </th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              User Name
            </th>
            {/* <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Father's Name</th>
                        <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Mother's Name</th> */}
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              User Email
            </th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              Bank Name
            </th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              Branch Name
            </th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              Status
            </th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              Actions
            </th>
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-200">
        {accounts
            .filter((account) => account.branchId === branchId)
            .map((account, index) => (
            <tr key={account._id}>
              <td className="px-6 py-4 whitespace-nowrap">{index + 1}</td>
              <td className="px-6 py-4 whitespace-nowrap">
                {account.branchId}
              </td>
              <td className="px-6 py-4 whitespace-nowrap">
                {account.userName}
              </td>
              {/* <td className="px-6 py-4 whitespace-nowrap">{account.fatherName}</td>
                            <td className="px-6 py-4 whitespace-nowrap">{account.motherName}</td> */}
              <td className="px-6 py-4 whitespace-nowrap">{account.email}</td>
              <td className="px-6 py-4 whitespace-nowrap">
                {account.selectedBank}
              </td>
              <td className="px-6 py-4 whitespace-nowrap">
                {account.bankAgentName}
              </td>
              <td className="px-6 py-4 whitespace-nowrap">
                {account.status === "Verified" ? (
                  <p className="text-green-500">Verified</p>
                ) : (
                  <p className="text-yellow-500">{account.status}</p>
                )}
              </td>
              <td className="px-6 py-4 whitespace-nowrap">
                {account.status !== "Verified" &&
                  account.status !== "Bank Verify" &&
                  account.status !== "Pending" && (
                    <button
                      onClick={() => openModal(account)}
                      className="bg-blue-500 btn-sm hover:bg-blue-700 text-xs text-white font-bold py-2 px-2 rounded"
                    >
                      Update Status
                    </button>
                  )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {/* Modal */}
      {selectedAccount && (
        <dialog className="modal bg-black bg-opacity-50" open={modalOpen}>
          <div className="modal-box max-w-fit">
            <form method="dialog">
              <button
                className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                onClick={closeModal}
              >
                ✕
              </button>
            </form>
            <h3 className="font-bold text-lg">Account Details</h3>
            <div className="w-96 md:w-[32rem] p-4">
              <div className="grid grid-cols-2 gap-4">
                <p className="font-semibold">Branch ID :</p>
                <p>{selectedAccount.branchId}</p>
                <p className="font-semibold">User Name :</p>
                <p>{selectedAccount.userName}</p>
                <p className="font-semibold">Father's Name :</p>
                <p>{selectedAccount.fatherName}</p>
                <p className="font-semibold">Mother's Name :</p>
                <p>{selectedAccount.motherName}</p>
                <p className="font-semibold">User Email :</p>
                <p>{selectedAccount.email}</p>
                <p className="font-semibold">Bank Name :</p>
                <p>{selectedAccount.selectedBank}</p>
                <p className="font-semibold">Branch Name :</p>
                <p>{selectedAccount.bankAgentName}</p>
                <p className="font-semibold">Address :</p>
                <p>{selectedAccount.address}</p>
                <p className="font-semibold">Insurance Type :</p>
                <p>{selectedAccount.insuranceType}</p>
                <p className="font-semibold">Duration :</p>
                <p>{selectedAccount.duration}</p>
              </div>
              {selectedAccount.status !== "Verified" && (
                <div>
                  <label htmlFor="statusSelect" className="font-semibold">Select Status :</label>
                  <select
                    id="statusSelect"
                    className="ml-36 p-2 border border-gray-300 rounded"
                    value={selectedAccount.status}
                    onChange={(e) => {
                      const status = e.target.value;
                      const bid = selectedAccount.bid; // Assuming `bid` is coming from the selectedAccount
                      const bankVerificationId =
                        status === "Bank Verify"
                          ? generateBankVerificationId()
                          : null;
                      handleStatusChange(
                        selectedAccount._id,
                        status,
                        bid,
                        bankVerificationId
                      );
                    }}
                  >
                    <option value="">Select</option>
                    <option value="Pending">Pending</option>
                    <option value="Bank Verify">Bank Verify</option>
                  </select>
                </div>
              )}
            </div>
          </div>
        </dialog>
      )}
    </div>
  );
};

export default BankDetails;
