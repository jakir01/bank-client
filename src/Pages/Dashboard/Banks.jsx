import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Banks = () => {
  const [loggedInUser, setLoggedInUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [showCreateBranchModal, setShowCreateBranchModal] = useState(false);
  const [branchFormData, setBranchFormData] = useState({
    branchName: "",
    email: "",
    password: "",
  });
  const [branchCounts, setBranchCounts] = useState({});
  const [user, setUser] = useState(null);
  const [branches, setBranches] = useState([]);
  const [errors, setErrors] = useState({});

  useEffect(() => {
    const storedUser = JSON.parse(localStorage.getItem("user"));
    if (storedUser) {
      setUser(storedUser);
      setLoggedInUser(storedUser);
    }
  }, []);

  useEffect(() => {
    if (loggedInUser) {
      setLoading(true);
      axios
        .get(`http://localhost:5000/branches?bankId=${loggedInUser.bankId}`)
        .then((response) => {
          setLoading(false);
          setBranches(response.data);
        })
        .catch((error) => {
          console.error("Error fetching branches:", error);
          setError("Failed to fetch branches. Please try again later.");
          setLoading(false);
        });
    }
  }, [loggedInUser]);

  const validateForm = () => {
    let tempErrors = {};
    let isValid = true;

    if (!branchFormData.branchName.trim()) {
      tempErrors.branchName = "Branch name is required";
      isValid = false;
    }
    if (!branchFormData.email.trim()) {
      tempErrors.email = "Email is required";
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(branchFormData.email)) {
      tempErrors.email = "Email is invalid";
      isValid = false;
    }
    if (!branchFormData.password.trim()) {
      tempErrors.password = "Password is required";
      isValid = false;
    } else if (branchFormData.password.length < 6) {
      tempErrors.password = "Password must be at least 6 characters long";
      isValid = false;
    }

    setErrors(tempErrors);
    return isValid;
  };

  const createBranch = () => {
    if (!loggedInUser) {
      console.error("No logged-in user.");
      return;
    }

    if (!validateForm()) {
      return;
    }

    const bankId = loggedInUser.bankId;
    const bankName = loggedInUser.bankName;

    // Retrieve the current branch count for the bank from local storage
    let currentBranchCount =
      parseInt(localStorage.getItem(`branchCount_${bankId}`)) || 0;

    // Increment the branch count for the bank
    currentBranchCount++;

    // Generate the branchId based on the bankId and current branch count
    const branchId = `${bankId}-${currentBranchCount}`;

    // Optimistically update branchCounts
    setBranchCounts((prevCounts) => ({
      ...prevCounts,
      [bankId]: currentBranchCount,
    }));

    axios
      .post("http://localhost:5000/branches", {
        bankId: bankId,
        bankName: bankName, // Include bank name in the request
        branchId: branchId,
        branchName: branchFormData.branchName,
        email: branchFormData.email,
        password: branchFormData.password,
        status: "", // Set default status to "inactive"
      })
      .then((response) => {
        console.log("Branch created successfully:", response.data);
        // Update local storage with the updated branch count
        localStorage.setItem(`branchCount_${bankId}`, currentBranchCount);
        // Update branches state with the new branch
        setBranches((prevBranches) => [...prevBranches, response.data]);
        // Close the modal
        setShowCreateBranchModal(false);
        window.location.reload();
      })
      .catch((error) => {
        console.error("Error creating branch:", error);
        // Rollback branchCounts state if branch creation fails
        setBranchCounts((prevCounts) => ({
          ...prevCounts,
          [bankId]: prevCounts[bankId] - 1, // Rollback to previous count
        }));
        // Handle error
      });

    setBranchFormData({
      branchName: "",
      email: "",
      password: "",
    });
  };

  const openCreateBranchModal = () => {
    setShowCreateBranchModal(true);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setBranchFormData({
      ...branchFormData,
      [name]: value,
    });
  };

  return (
    <div className="flex h-screen bg-gray-100">
      <div className="flex-1 p-10">
        {/* <h1 className="text-2xl font-bold mb-5">Bank Details</h1> */}
        {loading ? (
          <p>Loading...</p>
        ) : error ? (
          <p className="text-red-500">{error}</p>
        ) : user ? (
          <div className="bg-white p-8 border border-gray-200 rounded-lg shadow-sm">
            <h2 className="text-xl font-semibold">{user.bankName}</h2>
            <p>Branches: {branches.filter(branch => branch.bankId === loggedInUser.bankId).length}</p>
            <button
              onClick={openCreateBranchModal}
              className="mt-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            >
              Create a Branch
            </button>
            <div className="mt-4">
              <h3 className="text-lg font-semibold">Branches</h3>
              <div className="overflow-x-auto">
                <table className="mt-2 w-full border-collapse border border-gray-200">
                  <thead>
                    <tr className="bg-gray-100">
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Branch ID
                      </th>
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Branch Name
                      </th>
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Status
                      </th>
                    </tr>
                  </thead>
                  <tbody className="divide-y divide-gray-200">
  {branches
    .filter((branch) => branch.bankId === loggedInUser.bankId)
    .map((branch) => (
      <tr key={branch.branchId} className="text-sm">
        <td className="px-6 py-4 whitespace-nowrap">
          {branch.branchId}
        </td>
        <td className="px-6 py-4 whitespace-nowrap">
        {branch.status === "inactive" ? (
            <span>{branch.branchName}</span>
          ) : (
            <Link to={`/bankdetails/${branch.branchId}`}>
              {branch.branchName}
            </Link>
          )}
        </td>
        <td className="px-6 py-4 whitespace-nowrap">
          <span
            className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full ${
              branch.status === ""
                ? "bg-yellow-100 text-yellow-800"
                : branch.status === "active"
                ? "bg-green-100 text-green-800"
                : "bg-red-100 text-red-800"
            }`}
          >
            {branch.status === "" ? (
              "Pending"
            ) : branch.status === "inactive" ? (
              <span className="text-red-800">{branch.status}</span>
            ) : (
              branch.status
            )}
          </span>
        </td>
      </tr>
    ))}
</tbody>

                </table>
              </div>
            </div>
          </div>
        ) : (
          <p>No bank details available for the logged-in user.</p>
        )}
        {/* Modal code */}
        {showCreateBranchModal && (
        <div className="fixed z-10 inset-0 overflow-y-auto flex items-center justify-center">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
          <div className="relative bg-white rounded-lg w-96 p-6">
            <h2 className="text-xl font-semibold mb-4 bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500  bg-clip-text text-transparent">Create Branch</h2>
            <div className="mb-4">
              <label
                htmlFor="branchName"
                className="block text-sm font-medium text-gray-700"
              >
                Branch Name
              </label>
              <input
                type="text"
                name="branchName"
                id="branchName"
                autoComplete="off"
                className={`mt-1 p-2 border ${errors.branchName ? 'border-red-500' : 'border-gray-300'} rounded-md w-full`}
                value={branchFormData.branchName}
                onChange={handleInputChange}
              />
              {errors.branchName && <p className="text-red-500 text-xs italic">{errors.branchName}</p>}
            </div>
            <div className="mb-4">
              <label
                htmlFor="email"
                className="block text-sm font-medium text-gray-700"
              >
                Branch Email
              </label>
              <input
                type="email"
                name="email"
                id="email"
                autoComplete="off"
                className={`mt-1 p-2 border ${errors.email ? 'border-red-500' : 'border-gray-300'} rounded-md w-full`}
                value={branchFormData.email}
                onChange={handleInputChange}
              />
              {errors.email && <p className="text-red-500 text-xs italic">{errors.email}</p>}
            </div>
            <div className="mb-4">
              <label
                htmlFor="password"
                className="block text-sm font-medium text-gray-700"
              >
                Branch Password
              </label>
              <input
                type="password"
                name="password"
                id="password"
                autoComplete="off"
                className={`mt-1 p-2 border ${errors.password ? 'border-red-500' : 'border-gray-300'} rounded-md w-full`}
                value={branchFormData.password}
                onChange={handleInputChange}
              />
              {errors.password && <p className="text-red-500 text-xs italic">{errors.password}</p>}
            </div>
            <div className="flex justify-end">
              <button
                onClick={createBranch}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2"
              >
                Create
              </button>
              <button
                onClick={() => setShowCreateBranchModal(false)}
                className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded"
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      )}
      </div>
    </div>
  );
};

export default Banks;
