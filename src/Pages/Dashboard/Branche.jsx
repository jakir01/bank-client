import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

const Branche = () => {
  const [accounts, setAccounts] = useState([]);
  const [loading, setLoading] = useState(true);
  const { uid } = useParams();
  const [user, setUser] = useState(null);
  const [selectedAccount, setSelectedAccount] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    const loggedInUser = localStorage.getItem("user");
    if (loggedInUser) {
      setUser(JSON.parse(loggedInUser));
    }
  }, []);

  useEffect(() => {
    const userRole = localStorage.getItem("userRole");
    if (userRole === "admin" && user) {
      setUser({ ...user, role: "admin" });
    }
  }, [user]);

  useEffect(() => {
    if (user) {
      axios.get('http://localhost:5000/branches')
        .then(response => {
          const branchData = response.data;
          const userBranchStatus = branchData.find(branch => branch.branchName === user.branchName)?.status;
          setUser(prevUser => ({ ...prevUser, status: userBranchStatus }));
        })
        .catch(error => {
          console.error('There was an error fetching the branch status!', error);
        });
    }
  }, [user]);

  useEffect(() => {
    setLoading(true);
    axios
      .get(`http://localhost:5000/submits`)
      .then((response) => {
        setAccounts(response.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching accounts:", error);
        setLoading(false);
      });
  }, [uid]);

  const fetchAccounts = (uid) => {
    axios
      .get(`http://localhost:5000/submits/${uid}`)
      .then((response) => {
        setAccounts(response.data);
      })
      .catch((error) => {
        console.error("Error fetching accounts:", error);
      });
  };

  useEffect(() => {
    if (uid) {
      fetchAccounts(uid);
    }
  }, [uid]);

  // const handleStatusChange = (accountId, status) => {
  //   axios
  //     .patch(`http://localhost:5000/submits/${accountId}`, { status })
  //     .then((response) => {
  //       console.log('Status updated successfully:', response.data);
  //       setAccounts(
  //         accounts.map((account) =>
  //           account._id === accountId ? { ...account, status } : account
  //         )
  //       );
  //     })
  //     .catch((error) => {
  //       console.error('Error updating status:', error);
  //     });
  // };
  // Function to generate a random 8-digit bid
  const generateBid = () => {
    return Math.floor(10000000 + Math.random() * 90000000);
  };

  const handleStatusChange = (accountId, status) => {
    const bid = generateBid(); // Generate 8-digit unique ID (bid)

    axios
      .patch(`http://localhost:5000/submits/${accountId}`, { status, bid })
      .then((response) => {
        console.log("Status updated successfully:", response.data);
        // Update status and bid in local accounts state
        setAccounts(
          accounts.map((account) =>
            account._id === accountId ? { ...account, status, bid } : account
          )
        );
        // Close the modal after status update
        closeModal();
      })
      .catch((error) => {
        console.error("Error updating status:", error);
      });
  };

  const openModal = (account) => {
    setSelectedAccount(account);
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
    // Clear selected account after closing modal
    setSelectedAccount(null);
  };

  return (
    <div className="container mx-auto py-6">
      {user && (
        <h2>
          <div className="text-gray-900 font-semibold mb-4">
            {user.bankName}, {user.branchName}
            {user.status === "inactive" && (
              <p className="text-red-500">Inactive</p>
            )}
            {user.status === "active" && (
              <p className="text-green-500">Active</p>
            )}
          </div>
        </h2>
      )}
      {loading ? (
        <p>Loading...</p>
      ) : (
        <table className="min-w-full divide-y divide-gray-200">
          <thead className="bg-gray-50">
            <tr>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                S. No
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Agent Id
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                User Name
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Father's Name
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Mother's Name
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                User Email
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Bank Name
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Agent Name
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Status
              </th>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Actions
              </th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {user &&
              accounts
                .filter((account) => (account.branchId === user.branchId && account.bankId === user.bankId))
                .map((account, index) => (
                  <tr key={account.id}>
                    <td className="px-6 py-4 whitespace-nowrap">{index + 1}</td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.branchId}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.userName}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.fatherName}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.motherName}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.email}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.selectedBank}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.bankAgentName}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.status === "Verified" ? (
                        <p className="text-green-500">Verified</p>
                      ) : (
                        <p className="text-yellow-500">{account.status}</p>
                      )}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {account.status !== "Verified" &&
                        account.status !== "Branch Verify" &&
                        account.status !== "Bank Verify" && (
                          <button
                            onClick={() => openModal(account)}
                            className="bg-blue-500 btn-sm hover:bg-blue-700 text-xs text-white font-bold py-2 px-2 rounded"
                            disabled={user.status === "inactive"}
                          >
                            Update Status
                          </button>
                        )}
                    </td>
                  </tr>
                ))}
          </tbody>
        </table>
      )}

      {/* Modal */}
      {selectedAccount && (
        <dialog
          id="my_modal_3"
          className="modal bg-black bg-opacity-50"
          open={modalOpen}
        >
          <div className="modal-box max-w-fit max-h-fit">
            <form method="dialog">
              <button
                className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                onClick={closeModal}
              >
                ✕
              </button>
            </form>
            <h3 className="font-bold text-lg">Account Details</h3>
            {/* <p className="py-4">
              Press ESC key or click on ✕ button to close
            </p> */}
            <div className="w-96 md:w-[32rem] p-4">
              <div className="grid grid-cols-2 gap-4">
                <p className="font-semibold">Agent ID :</p>
                <p>{selectedAccount.branchId}</p>
                <p className="font-semibold">User Name :</p>
                <p>{selectedAccount.userName}</p>
                <p className="font-semibold">Father's Name :</p>
                <p>{selectedAccount.fatherName}</p>
                <p className="font-semibold">Mother's Name :</p>
                <p>{selectedAccount.motherName}</p>
                <p className="font-semibold">User Email :</p>
                <p>{selectedAccount.email}</p>
                <p className="font-semibold">Bank Name :</p>
                <p>{selectedAccount.selectedBank}</p>
                <p className="font-semibold">Branch Name :</p>
                <p>{selectedAccount.bankAgentName}</p>
                <p className="font-semibold">Address :</p>
                <p>{selectedAccount.address}</p>
                <p className="font-semibold">Insurance Type :</p>
                <p>{selectedAccount.insuranceType}</p>
                <p className="font-semibold">Duration :</p>
                <p>{selectedAccount.duration}</p>
                <p className="font-semibold">Amount :</p>
                <p>{selectedAccount.amount}</p>
                <p className="font-semibold">Payment :</p>
                <p>{selectedAccount.payment}</p>
              </div>
              {selectedAccount.status !== "Verified" && (
                <div className="mt-4 ">
                  <label htmlFor="statusSelect" className="font-semibold">
                    Select Status :
                  </label>
                  <select
                    id="statusSelect"
                    className="ml-36 p-2 border border-gray-300 rounded"
                    value={selectedAccount.status}
                    onChange={(e) =>
                      handleStatusChange(selectedAccount._id, e.target.value)
                    }
                    disabled={user.status === "inactive"}
                  >
                    <option value="Pending">Pending</option>
                    <option value="Branch Verify">Verify</option>
                  </select>
                </div>
              )}
            </div>
          </div>
        </dialog>
      )}
    </div>
  );
};

export default Branche;
