import React, { useState } from 'react';
import { jsPDF } from 'jspdf';

const MyAccounts = () => {
  const [userAccountId, setUserAccountId] = useState('');
  const [userData, setUserData] = useState(null);
  const [error, setError] = useState(null);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:5000/verifiedAccount");
      const data = await response.json();
      const matchedData = data.find(account => account.userAccountId === userAccountId);
      if (matchedData) {
        setUserData(matchedData);
        setError(null);
      } else {
        setUserData(null);
        setError('No account found for the provided ID.');
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setError('Error fetching data');
    }
  };

  const downloadPDF = () => {
    if (!userData) return;

    const doc = new jsPDF();

    // Add the header color shape
    doc.setFillColor(153, 102, 255); // Purple color
    doc.rect(0, 0, 210, 40, 'F');

    // Add the diagonal line (orange)
    doc.setFillColor(255, 102, 0); // Orange color
    doc.triangle(0, 0, 210, 0, 210, 20, 'F');

    // Title Section
    doc.setFontSize(22);
    doc.setTextColor(255, 255, 255);
    doc.setFont('helvetica', 'bold');
    doc.text(userData.selectedBank, 105, 20, { align: 'center' });

    // Add a placeholder for logo
    // doc.setTextColor(255, 255, 255);
    // doc.setFontSize(12);
    // doc.text('Brand Name', 180, 15, { align: 'right' });
    // doc.setFontSize(10);
    // doc.text('TAGLINE SPACE HERE', 180, 20, { align: 'right' });

    // Agent Information Section
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(16);
    doc.setFont('helvetica', 'bold');
    doc.text('Agent Information', 20, 50);
    doc.setFontSize(12);
    doc.setFont('helvetica', 'normal');
    doc.text(`Agent Name: ${userData.bankAgentName}`, 20, 60);
    doc.text(`Agent ID: ${userData.branchId}`, 20, 70);

    // Horizontal line after agent information
    doc.line(20, 80, 190, 80);

    // User Information Section
    doc.setFontSize(16);
    doc.setFont('helvetica', 'bold');
    doc.text('User Information', 20, 90);

    const startX = 20;
    const startY = 100;
    const lineHeight = 10;

    // Table headers
    const headers = ['', ''];
    const tableWidth = 170;
    const columnWidth = tableWidth / 2;

    doc.setFontSize(12);
    doc.setFont('helvetica', 'bold');
    doc.text(headers[0], startX, startY);
    doc.text(headers[1], startX + columnWidth, startY);

    // User data
    const data = [
        ['User Name', userData.userName],
        ['Father Name', userData.fatherName],
        ['Mother Name', userData.motherName],
        ['Date of Birth', userData.dateOfBirth],
        ['NID Number', userData.nidNumber],
        ['Phone Number', userData.phoneNumber],
        ['Email', userData.email]
    ];

    doc.setFont('helvetica', 'normal');
    data.forEach((item, index) => {
        doc.text(item[0], startX, startY + (index + 1) * lineHeight);
        doc.text(item[1], startX + columnWidth, startY + (index + 1) * lineHeight);
    });

    // Horizontal line after user information
    const userInfoEndY = startY + (data.length + 1) * lineHeight;
    doc.line(20, userInfoEndY + 10, 190, userInfoEndY + 10);

    // Verified Approved Account Section
    doc.setFontSize(16);
    doc.setFont('helvetica', 'bold');
    const verifiedAccountStartY = userInfoEndY + 30;
    doc.text('Verified Approved Account', 20, verifiedAccountStartY);

    doc.setFontSize(12);
    doc.setFont('helvetica', 'normal');
    const details = [
        ['Bank', userData.selectedBank],
        ['Insurance Type', userData.insuranceType],
        ['Duration', userData.duration],
        ['Amount', userData.amount],
        ['Payment', userData.payment],
        ['Account Status', userData.status]
    ];

    details.forEach((item, index) => {
        doc.text(`${item[0]}: ${item[1]}`, 20, verifiedAccountStartY + (index + 1) * lineHeight);
    });

    // Horizontal line after verified approved account section
    const verifiedAccountEndY = verifiedAccountStartY + (details.length + 1) * lineHeight;
    doc.line(20, verifiedAccountEndY + 5, 190, verifiedAccountEndY + 5);

    // Footer with thanks message
    // const footerY = 270;
    doc.setFontSize(10);
    doc.setFont('helvetica', 'normal');
    // doc.text('Thank you for your business', 20, footerY);
    doc.text(`Thanks from ${userData.selectedBank}`, 105, 290, { align: 'center' });

    // Save the PDF
    doc.save(`user_account_${userData.userAccountId}.pdf`);
};

  


  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4 text-center">My Approved Accounts</h1>
      <div className="mb-4">
        <input
          type="text"
          className="border rounded p-2 w-full"
          placeholder="Enter User Account ID"
          value={userAccountId}
          onChange={(e) => setUserAccountId(e.target.value)}
        />
        <button
          className="mt-2 bg-blue-500 text-white p-2 rounded w-full"
          onClick={fetchData}
        >
          Search
        </button>
      </div>
      {error && <p className="text-red-500">{error}</p>}
      {userData && (
        <div className="bg-gray-100 p-4 rounded shadow-md">
          <p><strong className='text-green-600'>Your Account is Approved:</strong></p>
          <p><strong>User Name:</strong> {userData.userName}</p>
          <p><strong>Father Name:</strong> {userData.fatherName}</p>
          <p><strong>Mother Name:</strong> {userData.motherName}</p>
          <p><strong>Date of Birth:</strong> {userData.dateOfBirth}</p>
          <p><strong>NID Number:</strong> {userData.nidNumber}</p>
          <p><strong>Phone Number:</strong> {userData.phoneNumber}</p>
          <p><strong>Email:</strong> {userData.email}</p>
          <p><strong>Bank:</strong> {userData.selectedBank}</p>
          <p><strong>Agent Name:</strong> {userData.bankAgentName}</p>
          <p><strong>Insurance Type:</strong> {userData.insuranceType}</p>
          <p><strong>Duration:</strong> {userData.duration}</p>
          <p><strong>Amount:</strong> {userData.amount}</p>
          <p><strong>Payment:</strong> <span className='text-green-600'>{userData.payment}</span></p>
          <p><strong>Account Status:</strong> <span className='text-green-600'>{userData.status}</span></p>
          <button
            className="mt-4 bg-blue-500 text-white p-2 rounded"
            onClick={downloadPDF}
          >
            Download as PDF
          </button>
        </div>
      )}
    </div>
  );
};

export default MyAccounts;
