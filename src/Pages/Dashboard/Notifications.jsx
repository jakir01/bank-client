import React, { useEffect, useState } from 'react';

const Notifications = () => {
  const [user, setUser] = useState(null);
  const [verifiedAccounts, setVerifiedAccounts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [copiedId, setCopiedId] = useState(null); // Track which ID was copied recently
  const [showCopied, setShowCopied] = useState(false); // State to control visibility of copied notification

  useEffect(() => {
    const loggedInUser = JSON.parse(localStorage.getItem("user"));
    
    if (loggedInUser) {
      setUser(loggedInUser);
      
      if (loggedInUser._id) {
        fetchVerifiedAccounts(loggedInUser._id);
      } else {
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  }, []);

  const fetchVerifiedAccounts = (userId) => {
    fetch(`http://localhost:5000/verifiedAccount/${userId}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setVerifiedAccounts(data); // Set the array of verified accounts
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching verified accounts:", error);
        setLoading(false);
      });
  };

  const copyToClipboard = (id) => {
    navigator.clipboard.writeText(id)
      .then(() => {
        console.log(`Copied ${id} to clipboard`);
        setCopiedId(id);
        setShowCopied(true);

        // Reset copied state after 5 seconds
        setTimeout(() => {
          setCopiedId(null);
          setShowCopied(false);
        }, 1500);
      })
      .catch((err) => {
        console.error('Error copying to clipboard: ', err);
      });
  };

  return (
    <div className="bg-gray-100 min-h-screen py-12">
      <div className="max-w-4xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="bg-white shadow-md rounded-lg overflow-hidden">
          <div className="px-6 py-4">
            <h1 className="text-3xl font-bold text-gray-800 mb-4">My Verified Insurance Id</h1>
            {loading ? (
              <p className="text-lg text-gray-600">Loading...</p>
            ) : verifiedAccounts.length > 0 ? (
              <div>
                <h2 className="text-xl font-semibold text-gray-800 mb-2">Verified Accounts:</h2>
                <ul className="divide-y divide-gray-200">
                  {verifiedAccounts.map((account) => (
                    <li key={account._id} className="flex items-center justify-between py-2">
                      <p className={`text-lg ${copiedId === account.userAccountId ? 'font-bold' : ''} text-gray-700`}>
                        User Account ID: {account.userAccountId}
                      </p>
                      <button
                        onClick={() => copyToClipboard(account.userAccountId)}
                        className="ml-4 px-3 py-1 rounded-md bg-blue-500 text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50"
                      >
                        Copy
                      </button>
                    </li>
                  ))}
                </ul>
              </div>
            ) : (
              <p className="text-lg text-gray-600">No verified accounts found for the logged-in user</p>
            )}

            {/* Notification for "Copied!" */}
            {showCopied && (
              <div className="mt-4 bg-green-100 border border-green-400 text-green-700 px-4 py-2 rounded-md flex items-center">
                <svg className="w-5 h-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7" />
                </svg>
                <span>Copied!</span>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Notifications;
