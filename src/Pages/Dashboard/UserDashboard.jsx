import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import MyAccounts from "./MyAccounts";
import Notifications from "./Notifications";
import {
  MdAccountCircle,
  MdList,
  MdNotifications,
  MdDescription,
} from "react-icons/md";

const UserDashboard = () => {
  const [user, setUser] = useState(null);
  const [submissions, setSubmissions] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedSubmission, setSelectedSubmission] = useState(null);
  const [amount, setAmount] = useState("");
  const [paymentSuccess, setPaymentSuccess] = useState(false);
  const [loadingPayment, setLoadingPayment] = useState(false);
  const [hasNewNotifications, setHasNewNotifications] = useState(true);
  const [activeSection, setActiveSection] = useState("submissions");

  useEffect(() => {
    // Check localStorage for notification state on component mount
    const storedNotificationState = localStorage.getItem("hasNewNotifications");
    if (storedNotificationState === "true") {
      setHasNewNotifications(true);
    } else {
      setHasNewNotifications(false);
    }
  }, []); // This effect runs only once on component mount

  const handleNotificationClick = () => {
    setHasNewNotifications(false); // Reset indicator when user clicks on Notifications link
    localStorage.setItem("hasNewNotifications", "false"); // Update localStorage
  };

  const handleNewNotification = () => {
    setHasNewNotifications(true); // Set indicator when new notifications arrive
    localStorage.setItem("hasNewNotifications", "true"); // Update localStorage
  };

  useEffect(() => {
    const loggedInUser = JSON.parse(localStorage.getItem("user"));
    if (loggedInUser) {
      setUser(loggedInUser);
    }

    if (loggedInUser && loggedInUser._id) {
      fetchSubmissions(loggedInUser._id);
    } else {
      setLoading(false);
    }
  }, []);

  const fetchSubmissions = (userId) => {
    fetch(`http://localhost:5000/submits/${userId}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setSubmissions(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching submissions:", error);
        setLoading(false);
      });
  };

  const handlePayClick = (submission) => {
    setSelectedSubmission(submission);
  };

  const closeModal = () => {
    setSelectedSubmission(null);
  };

  const handleAmountChange = (e) => {
    setAmount(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!amount || isNaN(parseFloat(amount))) {
      console.error("Invalid amount");
      // Handle invalid amount error (display message to user)
      return;
    }

    setLoadingPayment(true);
    const submissionId = selectedSubmission._id; // Using _id instead of uid
    try {
      const response = await fetch(
        `http://localhost:5000/submits/${submissionId}`,
        {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ payment: "Paid", amount: parseFloat(amount) }),
        }
      );

      if (!response.ok) {
        throw new Error("Failed to update payment");
      }

      console.log("Submission payment updated successfully");
      setPaymentSuccess(true);
      fetchSubmissions(user._id); // Refresh submissions after payment
      closeModal(); // Close modal after successful payment
    } catch (error) {
      console.error("Error updating submission payment:", error);
      // Handle error state or feedback to user
      // Display error message to user if needed
    } finally {
      setLoadingPayment(false); // Reset loading state
    }
  };

  if (!user) {
    return (
      <div className="min-h-screen flex items-center justify-center">
        <div className="text-gray-500 text-lg">Loading...</div>
      </div>
    );
  }

  return (
    <div className="min-h-screen bg-gray-100 flex">
      {/* Sidebar */}
      <div className="w-1/4 bg-gray-800 text-gray-300">
        <div className="p-4">
          <h2 className="text-2xl font-bold">User Dashboard</h2>
          <ul className="mt-4">
            <li
              className={`flex items-center p-2 mt-2 cursor-pointer hover:bg-gray-600 ${
                activeSection === "profile" && "bg-gray-600"
              }`}
              onClick={() => setActiveSection("profile")}
            >
              <MdAccountCircle className="text-gray-400 h-6 w-6 mr-2" />
              User Profile
            </li>
            <li
              className={`flex items-center p-2 mt-2 cursor-pointer hover:bg-gray-600 ${
                activeSection === "accounts" && "bg-gray-600"
              }`}
              onClick={() => setActiveSection("accounts")}
            >
              <MdList className="text-gray-400 h-6 w-6 mr-2" />
              My Accounts
            </li>
            <li
              className={`flex items-center p-2 mt-2 cursor-pointer hover:bg-gray-600 ${
                activeSection === "notifications" && "bg-gray-600"
              }`}
              onClick={() => {
                setActiveSection("notifications");
                handleNotificationClick();
              }}
            >
              <MdNotifications className={`text-gray-400 h-6 w-6 mr-2 ${hasNewNotifications ? 'text-red-500' : ''}`} />
              Notifications
              {hasNewNotifications && <span className="ml-1 bg-red-500 rounded-full px-2 py-1 text-xs font-bold">New</span>}
            </li>
            <li
              className={`flex items-center p-2 mt-2 cursor-pointer hover:bg-gray-600 ${
                activeSection === "submissions" && "bg-gray-600"
              }`}
              onClick={() => setActiveSection("submissions")}
            >
              <MdDescription className="text-gray-400 h-6 w-6 mr-2" />
              My Submissions
            </li>
          </ul>
        </div>
      </div>

      {/* Main Content */}
      <div className="w-3/4 p-6">
        {activeSection === "profile" && (
          <div>
            <h2 className="text-3xl font-bold">User Profile</h2>
            <p className="mt-2">Email: {user.email}</p>
            <p className="mt-2">Username: {user.username}</p>
          </div>
        )}
        {activeSection === "accounts" && (
          <div>
            <h2 className="text-3xl font-bold">My Accounts</h2>
            <p className="mt-2">Search by User account Id is Verified or not</p>
            <MyAccounts />
          </div>
        )}
        {activeSection === "notifications" && (
          <div>
            <h2 className="text-3xl font-bold">Notifications</h2>
            <p className="mt-2">All new notifications find here.</p>
            <Notifications onNewNotification={handleNewNotification} />
            {/* <Notifications /> */}
          </div>
        )}
        {activeSection === "submissions" && (
          <div>
            <h2 className="text-3xl font-bold">My Submissions</h2>
            {loading ? (
              <div className="text-center mt-4">Loading submissions...</div>
            ) : (
              <div className="mt-8">
                <div className="overflow-x-auto">
                  <table className="min-w-full bg-white rounded-lg overflow-hidden">
                    <thead className="bg-gray-200 text-gray-800 uppercase text-sm leading-normal">
                      <tr>
                        <th className="py-3 px-6 text-left">Sl. No</th>
                        <th className="py-3 px-6 text-left">User Name</th>
                        <th className="py-3 px-6 text-left">User Email</th>
                        <th className="py-3 px-6 text-left">Bank Name</th>
                        <th className="py-3 px-6 text-left">Agent Name</th>
                        <th className="py-3 px-6 text-left">Amount</th>
                        <th className="py-3 px-6 text-left">Actions</th>
                      </tr>
                    </thead>
                    <tbody className="text-gray-700">
                      {submissions.map((submission, index) => (
                        <tr key={index}>
                          <td className="py-3 px-6 text-left">{index + 1}</td>
                          <td className="py-3 px-6 text-left">
                            {submission.userName}
                          </td>
                          <td className="py-3 px-6 text-left">
                            {submission.email}
                          </td>
                          <td className="py-3 px-6 text-left">
                            {submission.selectedBank}
                          </td>
                          <td className="py-3 px-6 text-left">
                            {submission.bankAgentName}
                          </td>
                          <td className="py-3 px-6 text-left">
                          {submission.amount || "00"}
                          </td>
                          <td className="py-3 px-6 text-left">
                            {submission.payment === "Paid" ? (
                              <button
                                className="bg-green-500 text-white px-4 py-2 rounded-lg"
                                disabled
                              >
                                Paid
                              </button>
                            ) : (
                              <button
                                className="bg-blue-500 text-white px-4 py-2 rounded-lg"
                                onClick={() => handlePayClick(submission)}
                              >
                                Pay
                              </button>
                            )}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            )}
          </div>
        )}
      </div>

      {/* Modal for Submission Details */}
      {selectedSubmission && (
        <div className="fixed z-10 inset-0 overflow-y-auto">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <form onSubmit={handleSubmit}>
                <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                  <div className="sm:flex sm:items-start">
                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                      <h3 className="text-lg leading-6 font-medium text-gray-900">
                        Submission Details
                      </h3>
                      <div className="mt-2">
                        <div className="grid grid-cols-2 gap-4">
                          <p className="font-semibold">Form ID :</p>
                          <p>{selectedSubmission.formId}</p>
                          <p className="font-semibold">User Name :</p>
                          <p>{selectedSubmission.userName}</p>
                          <p className="font-semibold">Father's Name :</p>
                          <p>{selectedSubmission.fatherName}</p>
                          <p className="font-semibold">Mother's Name :</p>
                          <p>{selectedSubmission.motherName}</p>
                          <p className="font-semibold">User Email :</p>
                          <p>{selectedSubmission.email}</p>
                          <p className="font-semibold">Bank Name :</p>
                          <p>{selectedSubmission.selectedBank}</p>
                          <p className="font-semibold">Agent Name :</p>
                          <p>{selectedSubmission.bankAgentName}</p>
                          <p className="font-semibold">Insurance Type :</p>
                          <p>{selectedSubmission.insuranceType}</p>
                          <p className="font-semibold">Deposit Amount :</p>
                          <p>{selectedSubmission.amount}</p>
                          <p className="font-semibold">Payment :</p>
                          <p>{selectedSubmission.payment}</p>
                          <p className="font-semibold">Duration :</p>
                          <p>{selectedSubmission.duration}</p>
                        </div>
                        <div className="mt-4">
                          <label
                            htmlFor="amountInput"
                            className="font-semibold"
                          >
                            Amount:
                          </label>
                          <input
                            id="amountInput"
                            type="text"
                            className="ml-2 p-2 border border-gray-300 rounded"
                            value={amount}
                            onChange={handleAmountChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                  <button
                    type="submit"
                    className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 text-base font-medium text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    {loadingPayment ? "Processing..." : "Payment"}
                  </button>
                  <button
                    type="button"
                    onClick={closeModal}
                    className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                    disabled={loadingPayment}
                  >
                    Close
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}

      {/* Payment Success Message */}
      {paymentSuccess && (
        <div className="fixed z-10 inset-0 overflow-y-auto">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <div className="inline-block align-middle bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="text-center">
                  <h3 className="text-lg leading-6 font-medium text-gray-900">
                    Payment Successful
                  </h3>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      The payment was successfully processed.
                    </p>
                  </div>
                </div>
                <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                  <button
                    onClick={() => setPaymentSuccess(false)}
                    className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 text-base font-medium text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default UserDashboard;
