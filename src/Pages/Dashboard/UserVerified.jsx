import React, { useState, useEffect } from "react";

const UserVerified = () => {
  const [userData, setUserData] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Function to fetch data from the endpoint
  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:5000/verifiedAccount");
      const data = await response.json();
      setUserData(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  // Function to handle opening the modal and selecting user data
  const openModal = (user) => {
    setSelectedUser(user);
    setIsModalOpen(true);
  };

  // Function to handle closing the modal
  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="container mx-auto max-w-fit p-4">
      <div className="overflow-x-auto relative shadow-md sm:rounded-lg">
        <h1 className="text-2xl text-stone-500 text-center mt-10 mb-10">
          Verified FDR Accounts
        </h1>
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="py-3 px-6">
                Insurance ID
              </th>
              <th scope="col" className="py-3 px-6">
                Name
              </th>
              <th scope="col" className="py-3 px-6">
                Email
              </th>
              <th scope="col" className="py-3 px-6">
                Bank
              </th>
              <th scope="col" className="py-3 px-6">
                Bank Agent
              </th>
              <th scope="col" className="py-3 px-6">
                Address
              </th>
              <th scope="col" className="py-3 px-6">
                Insurance Type
              </th>
              <th scope="col" className="py-3 px-6">
                Duration
              </th>
              <th scope="col" className="py-3 px-6">
                Amount
              </th>
              <th scope="col" className="py-3 px-6">
                Payment
              </th>
              <th scope="col" className="py-3 px-6">
                Status
              </th>
              <th scope="col" className="py-3 px-6">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {userData.map((user) => (
              <tr
                key={user._id}
                className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 cursor-pointer"
                onClick={() => openModal(user)}
              >
                <td className="py-4 px-6">{user.userAccountId}</td>
                <td className="py-4 px-6">{user.userName}</td>
                <td className="py-4 px-6">{user.email}</td>
                <td className="py-4 px-6">{user.selectedBank}</td>
                <td className="py-4 px-6">{user.bankAgentName}</td>
                <td className="py-4 px-6">{user.address}</td>
                <td className="py-4 px-6">{user.insuranceType}</td>
                <td className="py-4 px-6">{user.duration}</td>
                <td className="py-4 px-6">{user.amount}</td>
                <td className="py-4 px-6">{user.payment}</td>
                <td className="py-4 px-6 text-green-500">{user.status}</td>
                <td className="py-4 px-6 text-green-500">
                  <button
                            onClick={() => openModal()}
                            className="mt-4 py-2 px-4 bg-gradient-to-r from-blue-500 to-green-500 hover:from-pink-500 hover:to-yellow-500 text-white rounded-md"
                          >
                            View
                          </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* Modal */}
      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-white rounded-lg p-8 w-1/2">
            <h2 className="text-2xl text-gray-800 mb-4">User Details</h2>
            <div>
              <p>ID: {selectedUser.userAccountId}</p>
              <p>Name: {selectedUser.userName}</p>
              <p>Email: {selectedUser.email}</p>
              <p>Bank: {selectedUser.selectedBank}</p>
              <p>Bank Agent: {selectedUser.bankAgentName}</p>
              <p>Address: {selectedUser.address}</p>
              <p>Insurance Type: {selectedUser.insuranceType}</p>
              <p>Duration: {selectedUser.duration}</p>
              <p>Amount: {selectedUser.amount}</p>
              <p>Payment: {selectedUser.payment}</p>
              <p>Status: {selectedUser.status}</p>
            </div>
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-4"
              onClick={closeModal}
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default UserVerified;
