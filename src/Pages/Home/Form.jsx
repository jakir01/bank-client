import { useEffect, useState } from "react";
import axios from "axios";
import { Toaster, toast } from "react-hot-toast";

const FdrAccountForm = () => {
  const [formData, setFormData] = useState({
    formId: "",
    userName: "",
    fatherName: "",
    motherName: "",
    dateOfBirth: "",
    nidNumber: "",
    phoneNumber: "",
    email: "",
    selectedBank: "",
    bankId: "", // New field for storing the bank ID
    bankAgentName: "",
    branchId: "",
    status: "Pending",
    address: "",
    insuranceType: "",
    duration: "",
  });

  const [currentStep, setCurrentStep] = useState(0);
  const [bankBranches, setBankBranches] = useState([]);
  const [banks, setBanks] = useState([]);
  const [errors, setErrors] = useState({});
  const [bankAgentOptions, setBankAgentOptions] = useState([]);

  useEffect(() => {
    fetchBanks();
    fetchBankBranches();
  }, []);

  const fetchBanks = async () => {
    try {
      const response = await axios.get("http://localhost:5000/bank");
      setBanks(response.data);
    } catch (error) {
      console.error("Error fetching banks:", error);
    }
  };

  const fetchBankBranches = async () => {
    try {
      const response = await axios.get("http://localhost:5000/branches");
      setBankBranches(response.data);
    } catch (error) {
      console.error("Error fetching bank branches:", error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "selectedBank") {
      const selectedBank = banks.find((bank) => bank.bankName === value);
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
        bankId: selectedBank?.bankId || "", // Adding bankId to formData
        bankAgentName: "",
        branchId: "",
      }));
      const selectedBankBranches = bankBranches
        .filter((branch) => branch.bankName === value)
        .map((branch) => branch.branchName);
      setBankAgentOptions(selectedBankBranches);
    } else if (name === "bankAgentName") {
      const branch = bankBranches.find(
        (branch) =>
          branch.bankName === formData.selectedBank &&
          branch.branchName === value
      );
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
        branchId: branch?.branchId || "",
      }));
    } else {
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
  };

  const validateInput = (value) => /^[a-zA-Z\s]*$/.test(value);

  const validateNidNumber = (value) => /^\d{12}$/.test(value);

  const validatePhoneNumber = (value) => /^\d{11}$/.test(value);

  const validateEmail = (email) => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

  const validateForm = () => {
    let tempErrors = {};
    let isValid = true;

    if (!formData.userName.trim() || !validateInput(formData.userName)) {
      tempErrors.userName = "Invalid user name";
      isValid = false;
    }
    if (!formData.fatherName.trim() || !validateInput(formData.fatherName)) {
      tempErrors.fatherName = "Invalid father's name";
      isValid = false;
    }
    if (!formData.motherName.trim() || !validateInput(formData.motherName)) {
      tempErrors.motherName = "Invalid mother's name";
      isValid = false;
    }
    if (!formData.dateOfBirth.trim()) {
      tempErrors.dateOfBirth = "Date of birth is required";
      isValid = false;
    }
    if (!formData.nidNumber.trim() || !validateNidNumber(formData.nidNumber)) {
      tempErrors.nidNumber = "NID number must be exactly 12 digits";
      isValid = false;
    }
    if (
      !formData.phoneNumber.trim() ||
      !validatePhoneNumber(formData.phoneNumber)
    ) {
      tempErrors.phoneNumber = "Phone number must be 11 digits";
      isValid = false;
    }
    if (!formData.email.trim() || !validateEmail(formData.email)) {
      tempErrors.email = "Invalid email address";
      isValid = false;
    }
    if (!formData.selectedBank.trim()) {
      tempErrors.selectedBank = "Please select an insurance company";
      isValid = false;
    }
    if (!formData.bankAgentName.trim()) {
      tempErrors.bankAgentName = "Please select an insurance branch";
      isValid = false;
    }
    if (!formData.address.trim()) {
      tempErrors.address = "Address is required";
      isValid = false;
    }
    if (!formData.insuranceType.trim()) {
      tempErrors.insuranceType = "Please select an insurance type";
      isValid = false;
    }
    if (!formData.duration.trim()) {
      tempErrors.duration = "Please select a duration";
      isValid = false;
    }

    setErrors(tempErrors);
    return isValid;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    const formId = generateFormId();

    const user = JSON.parse(localStorage.getItem("user"));
    if (!user || !user._id) {
      console.error("User data not found in localStorage");
      toast.error("User data not found. Please log in again.");
      return;
    }

    try {
      const submitResponse = await axios.post("http://localhost:5000/submits", {
        ...formData,
        formId: formId,
        bankAgentStatus: "",
        uid: user._id,
      });
      console.log(submitResponse.data);

      setFormData({
        formId: "",
        userName: "",
        fatherName: "",
        motherName: "",
        dateOfBirth: "",
        nidNumber: "",
        phoneNumber: "",
        email: "",
        selectedBank: "",
        bankId: "", // Reset bankId
        bankAgentName: "",
        branchId: "",
        status: "Pending",
        address: "",
        insuranceType: "",
        duration: "",
      });

      toast.success("Form submitted successfully!");
      setCurrentStep(0);
    } catch (error) {
      console.error("Submission error:", error);
      toast.error("Failed to submit the form.");
    }
  };

  const generateFormId = () => {
    const timestamp = Date.now();
    const timestampPart = timestamp % 100000;
    const randomPart = Math.floor(Math.random() * 900) + 100;
    return timestampPart * 1000 + randomPart;
  };

  const handleNextStep = () => {
    if (currentStep === 0 && !validateStepOne()) {
      return;
    }
    if (currentStep === 1 && !validateStepTwo()) {
      return;
    }
    setCurrentStep(currentStep + 1);
  };

  const handlePreviousStep = () => {
    setCurrentStep(currentStep - 1);
  };

  const validateStepOne = () => {
    let tempErrors = {};
    let isValid = true;

    if (!formData.userName.trim() || !validateInput(formData.userName)) {
      tempErrors.userName = "Invalid user name";
      isValid = false;
    }
    if (!formData.fatherName.trim() || !validateInput(formData.fatherName)) {
      tempErrors.fatherName = "Invalid father's name";
      isValid = false;
    }
    if (!formData.motherName.trim() || !validateInput(formData.motherName)) {
      tempErrors.motherName = "Invalid mother's name";
      isValid = false;
    }
    if (!formData.dateOfBirth.trim()) {
      tempErrors.dateOfBirth = "Date of birth is required";
      isValid = false;
    }
    if (!formData.nidNumber.trim() || !validateNidNumber(formData.nidNumber)) {
      tempErrors.nidNumber = "NID number must be exactly 12 digits";
      isValid = false;
    }
    if (
      !formData.phoneNumber.trim() ||
      !validatePhoneNumber(formData.phoneNumber)
    ) {
      tempErrors.phoneNumber = "Phone number must be 11 digits";
      isValid = false;
    }
    if (!formData.email.trim() || !validateEmail(formData.email)) {
      tempErrors.email = "Invalid email address";
      isValid = false;
    }
    if (!formData.address.trim()) {
      tempErrors.address = "Address is required";
      isValid = false;
    }

    setErrors(tempErrors);
    return isValid;
  };

  const validateStepTwo = () => {
    let tempErrors = {};
    let isValid = true;

    if (!formData.selectedBank.trim()) {
      tempErrors.selectedBank = "Please select an insurance company";
      isValid = false;
    }
    if (!formData.bankAgentName.trim()) {
      tempErrors.bankAgentName = "Please select an insurance branch";
      isValid = false;
    }
    if (!formData.insuranceType.trim()) {
      tempErrors.insuranceType = "Please select an insurance type";
      isValid = false;
    }
    if (!formData.duration.trim()) {
      tempErrors.duration = "Please select a duration";
      isValid = false;
    }

    setErrors(tempErrors);
    return isValid;
  };

  return (
    <div className="max-w-3xl mx-auto my-10 p-6 bg-white rounded-lg shadow-md">
      <Toaster />
      <h2 className="text-2xl font-bold mb-6 text-center bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 bg-clip-text text-transparent">
        Create Insurance Account
      </h2>
      <form
        onSubmit={handleSubmit}
        className="max-w-2xl mx-auto p-6 bg-white rounded-md shadow-md"
      >
        {currentStep === 0 && (
          <div>
            <h2 className="text-2xl font-semibold text-gray-700 mb-4">
              Personal Information
            </h2>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                User Name
              </label>
              <input
                type="text"
                name="userName"
                value={formData.userName}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter User Name"
              />
              {errors.userName && (
                <span className="text-red-500 text-sm">{errors.userName}</span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Father's Name
              </label>
              <input
                type="text"
                name="fatherName"
                value={formData.fatherName}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter Father's Name"
              />
              {errors.fatherName && (
                <span className="text-red-500 text-sm">
                  {errors.fatherName}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Mother's Name
              </label>
              <input
                type="text"
                name="motherName"
                value={formData.motherName}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter Mother's Name"
              />
              {errors.motherName && (
                <span className="text-red-500 text-sm">
                  {errors.motherName}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Date of Birth
              </label>
              <input
                type="date"
                name="dateOfBirth"
                value={formData.dateOfBirth}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
              />
              {errors.dateOfBirth && (
                <span className="text-red-500 text-sm">
                  {errors.dateOfBirth}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                NID Number
              </label>
              <input
                type="text"
                name="nidNumber"
                value={formData.nidNumber}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter NID Number"
              />
              {errors.nidNumber && (
                <span className="text-red-500 text-sm">{errors.nidNumber}</span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Phone Number
              </label>
              <input
                type="text"
                name="phoneNumber"
                value={formData.phoneNumber}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter Your Phone Number"
              />
              {errors.phoneNumber && (
                <span className="text-red-500 text-sm">
                  {errors.phoneNumber}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Email
              </label>
              <input
                type="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter Your Email"
              />
              {errors.email && (
                <span className="text-red-500 text-sm">{errors.email}</span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Address
              </label>
              <input
                type="text"
                name="address"
                value={formData.address}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
                placeholder="Enter Your Address"
              />
              {errors.address && (
                <span className="text-red-500 text-sm">{errors.address}</span>
              )}
            </div>
            <div className="flex justify-end">
              <button
                type="button"
                onClick={handleNextStep}
                className="px-4 py-2 bg-indigo-500 text-white rounded-md hover:bg-indigo-600"
              >
                Next
              </button>
            </div>
          </div>
        )}

        {currentStep === 1 && (
          <div>
            <h2 className="text-2xl font-semibold text-gray-700 mb-4">
              Insurance Details
            </h2>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Select Insurance Company
              </label>
              <select
                name="selectedBank"
                value={formData.selectedBank}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
              >
                <option value="">Select Bank</option>
                {banks.map((bank) => (
                  <option key={bank.bankId} value={bank.bankName}>
                    {bank.bankName}
                  </option>
                ))}
              </select>
              {errors.selectedBank && (
                <span className="text-red-500 text-sm">
                  {errors.selectedBank}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Select Insurance Agent
              </label>
              <select
                name="bankAgentName"
                value={formData.bankAgentName}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
              >
                <option value="">Select Agent</option>
                {bankAgentOptions.map((branchName, index) => (
                  <option key={index} value={branchName}>
                    {branchName}
                  </option>
                ))}
              </select>
              {errors.bankAgentName && (
                <span className="text-red-500 text-sm">
                  {errors.bankAgentName}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Select Insurance Type
              </label>
              <select
                name="insuranceType"
                value={formData.insuranceType}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
              >
                <option value="">Select Insurance Type</option>
                <option value="Life Insurance">Life Insurance</option>
                <option value="Health Insurance">Health Insurance</option>
                <option value="Property Insurance">Property Insurance</option>
                <option value="Vehicle Insurance">Vehicle Insurance</option>
              </select>
              {errors.insuranceType && (
                <span className="text-red-500 text-sm">
                  {errors.insuranceType}
                </span>
              )}
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-medium mb-2">
                Duration
              </label>
              <select
                name="duration"
                value={formData.duration}
                onChange={handleChange}
                className="w-full border-gray-300 rounded-md shadow-sm p-2"
              >
                <option value="">Select Duration</option>
                <option value="1 Year">1 Year</option>
                <option value="2 Years">2 Years</option>
                <option value="3 Years">3 Years</option>
                <option value="5 Years">5 Years</option>
                <option value="10 Years">10 Years</option>
              </select>
              {errors.duration && (
                <span className="text-red-500 text-sm">{errors.duration}</span>
              )}
            </div>
            <div className="flex justify-between">
              <button
                type="button"
                onClick={handlePreviousStep}
                className="px-4 py-2 bg-gray-500 text-white rounded-md hover:bg-gray-600"
              >
                Back
              </button>
              <button
                type="button"
                onClick={handleNextStep}
                className="px-4 py-2 bg-indigo-500 text-white rounded-md hover:bg-indigo-600"
              >
                Next
              </button>
            </div>
          </div>
        )}

        {currentStep === 2 && (
          <div>
            <h2 className="text-2xl font-semibold text-gray-700 mb-4">
              Summary
            </h2>
            <div className="p-4 border rounded-md bg-gray-50">
              <p>
                <strong>User Name:</strong> {formData.userName}
              </p>
              <p>
                <strong>Father's Name:</strong> {formData.fatherName}
              </p>
              <p>
                <strong>Mother's Name:</strong> {formData.motherName}
              </p>
              <p>
                <strong>Date of Birth:</strong> {formData.dateOfBirth}
              </p>
              <p>
                <strong>NID Number:</strong> {formData.nidNumber}
              </p>
              <p>
                <strong>Phone Number:</strong> {formData.phoneNumber}
              </p>
              <p>
                <strong>Email:</strong> {formData.email}
              </p>
              <p>
                <strong>Address:</strong> {formData.address}
              </p>
              <p>
                <strong>Selected Insurance Company:</strong>{" "}
                {formData.selectedBank}
              </p>
              <p>
                <strong>Selected Insurance Agent:</strong>{" "}
                {formData.bankAgentName}
              </p>
              <p>
                <strong>Insurance Type:</strong> {formData.insuranceType}
              </p>
              <p>
                <strong>Duration:</strong> {formData.duration}
              </p>
            </div>
            <div className="flex justify-between mt-4">
              <button
                type="button"
                onClick={handlePreviousStep}
                className="px-4 py-2 bg-gray-500 text-white rounded-md hover:bg-gray-600"
              >
                Back
              </button>
              <button
                type="submit"
                className="px-4 py-2 bg-indigo-500 text-white rounded-md hover:bg-indigo-600"
              >
                Submit
              </button>
            </div>
          </div>
        )}
      </form>
    </div>
  );
};

export default FdrAccountForm;
