import { useEffect, useState } from "react";
import signin from "../../assets/signin-image.webp"

const AuthForm = () => {
  const [formState, setFormState] = useState({
    email: "",
    password: "",
    role: "",
    bankName: "",
    userName: "",
  });
  const [errors, setErrors] = useState({});
  const [user, setUser] = useState(null); // State to store user information

  useEffect(() => {
    const loggedInUser = localStorage.getItem("user");
    if (loggedInUser) {
      setUser(JSON.parse(loggedInUser));
    }
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const validateForm = () => {
    let tempErrors = {};
    if (!formState.email.trim()) tempErrors.email = "Email is required";
    else if (!/\S+@\S+\.\S+/.test(formState.email))
      tempErrors.email = "Email is invalid";
    if (!formState.password) tempErrors.password = "Password is required";
    else if (formState.password.length < 6)
      tempErrors.password = "Password must be at least 6 characters long";
    if (formState.role === "") tempErrors.role = "Role is required";

    setErrors(tempErrors);
    return Object.keys(tempErrors).length === 0;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        let endpoint = "";
        let redirectUrl = ""; // URL to redirect after successful login
        let userRole = ""; // Role of the user

        if (formState.role === "bank") {
          endpoint = "http://localhost:5000/login";
          redirectUrl = "http://localhost:5173/banks";
          userRole = "bank";
        } else if (formState.role === "admin") {
          endpoint = "http://localhost:5000/admin/login";
          redirectUrl = "http://localhost:5173/bangladeshbank";
          userRole = "admin";
        } else if (formState.role === "branch") {
          endpoint = "http://localhost:5000/branches/login";
          redirectUrl = "http://localhost:5173/branch";
          userRole = "branch";
        } else if (formState.role === "user") {
          endpoint = "http://localhost:5000/user/login";
          redirectUrl = "http://localhost:5173/userdashboard";
          userRole = "user";
        }

        if (endpoint !== "") {
          const response = await fetch(endpoint, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: formState.email,
              password: formState.password,
            }),
          });

          if (!response.ok) {
            throw new Error("Failed to log in");
          }

          const data = await response.json();
          console.log("Login successful:", data);

          setUser(data.user);

          if (userRole) {
            localStorage.setItem("userRole", userRole);
          }

          // Redirect to the appropriate page based on the user's role
          window.location.href = redirectUrl;
        }
      } catch (error) {
        console.error("Error logging in:", error.message);
        // Handle error (e.g., display error message)
      }
    } else {
      console.log("Form has errors");
    }
  };

  useEffect(() => {
    if (user) {
      localStorage.setItem("user", JSON.stringify(user));
    } else {
      localStorage.removeItem("user");
    }
  }, [user]); // Update localStorage whenever the user state changes

  // Redirect to signup page
  const handleCreateAccount = () => {
    window.location.href = "http://localhost:5173/signup";
  };

  return (
    <div className="min-h-screen flex flex-col items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-6xl w-full grid md:grid-cols-2 gap-4 shadow-[0_2px_10px_-3px_rgba(6,81,237,0.3)] rounded-md p-4 m-4">
        <div className="md:max-w-md w-full sm:px-6 py-4">
          <form onSubmit={handleSubmit}>
            <div className="mb-12">
              <h3 className="text-3xl font-extrabold">Sign in</h3>
              <p className="text-sm mt-4">
                Don't have an account{" "}
                <a
                  href="javascript:void(0);"
                  onClick={handleCreateAccount}
                  className="text-blue-600 font-semibold hover:underline ml-1 whitespace-nowrap"
                >
                  Register here
                </a>
              </p>
            </div>
            <div>
              <label className="text-xs block mb-2">Role</label>
              <select
                name="role"
                value={formState.role}
                onChange={handleChange}
                className="w-full text-sm border-b border-gray-300 focus:border-[#333] px-2 py-3 outline-none"
              >
                <option value="">Select role</option>
                <option value="user">User</option>
                <option value="branch">Agent</option>
                <option value="bank">Insurance</option>
                <option value="admin">BD Bank</option>
              </select>
              {errors.role && (
                <p className="text-red-500 text-xs italic">{errors.role}</p>
              )}
            </div>
            <div className="mt-8">
              <label className="text-xs block mb-2">Email</label>
              <div className="relative flex items-center">
                <input
                  name="email"
                  type="text"
                  required
                  value={formState.email}
                  onChange={handleChange}
                  className="w-full text-sm border-b border-gray-300 focus:border-[#333] px-2 py-3 outline-none"
                  placeholder="Enter email"
                />
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="#bbb"
                  stroke="#bbb"
                  className="w-[18px] h-[18px] absolute right-2"
                  viewBox="0 0 682.667 682.667"
                >
                  <defs>
                    <clipPath id="a" clipPathUnits="userSpaceOnUse">
                      <path
                        d="M0 512h512V0H0Z"
                        data-original="#000000"
                      ></path>
                    </clipPath>
                  </defs>
                  <g
                    clipPath="url(#a)"
                    transform="matrix(1.33 0 0 -1.33 0 682.667)"
                  >
                    <path
                      fill="none"
                      stroke-miterlimit="10"
                      strokeWidth="40"
                      d="M452 444H60c-22.091 0-40-17.909-40-40v-39.446l212.127-157.782c14.17-10.54 33.576-10.54 47.746 0L492 364.554V404c0 22.091-17.909 40-40 40Z"
                      data-original="#000000"
                    ></path>
                    <path
                      d="M472 274.9V107.999c0-11.027-8.972-20-20-20H60c-11.028 0-20 8.973-20 20V274.9L0 304.652V107.999c0-33.084 26.916-60 60-60h392c33.084 0 60 26.916 60 60v196.653Z"
                      data-original="#000000"
                    ></path>
                  </g>
                </svg>
              </div>
              {errors.email && (
                <p className="text-red-500 text-xs italic">{errors.email}</p>
              )}
            </div>
            <div className="mt-8">
              <label className="text-xs block mb-2">Password</label>
              <div className="relative flex items-center">
                <input
                  name="password"
                  type="password"
                  required
                  value={formState.password}
                  onChange={handleChange}
                  className="w-full text-sm border-b border-gray-300 focus:border-[#333] px-2 py-3 outline-none"
                  placeholder="Enter password"
                />
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="#bbb"
                  stroke="#bbb"
                  className="w-[18px] h-[18px] absolute right-2 cursor-pointer"
                  viewBox="0 0 128 128"
                >
                  <path
                    d="M64 104C22.127 104 1.367 67.496.504 65.943a4 4 0 0 1 0-3.887C1.367 60.504 22.127 24 64 24s62.633 36.504 63.496 38.057a4 4 0 0 1 0 3.887C126.633 67.496 105.873 104 64 104zM8.707 63.994C13.465 71.205 32.146 96 64 96c31.955 0 50.553-24.775 55.293-31.994C114.535 56.795 95.854 32 64 32 32.045 32 13.447 56.775 8.707 63.994zM64 88c-13.233 0-24-10.767-24-24s10.767-24 24-24 24 10.767 24 24-10.767 24-24 24zm0-40c-8.822 0-16 7.178-16 16s7.178 16 16 16 16-7.178 16-16-7.178-16-16-16z"
                    data-name="Glyph"
                  ></path>
                </svg>
              </div>
              {errors.password && (
                <p className="text-red-500 text-xs italic">{errors.password}</p>
              )}
            </div>            
            <div className="mt-8 flex justify-between">
              <div className="flex items-center gap-2">
                <input
                  id="remember"
                  type="checkbox"
                  className="h-3 w-3 cursor-pointer accent-[#2A9D8F]"
                />
                <label
                  htmlFor="remember"
                  className="text-xs cursor-pointer"
                >
                  Remember me
                </label>
              </div>
              <a
                href="javascript:void(0);"
                className="text-xs text-blue-600 hover:underline"
              >
                Forgot password?
              </a>
            </div>
            <div className="mt-8">
              <button
                type="submit"
                className="w-full bg-[#333] text-white py-3 rounded-md text-sm hover:bg-[#222] transition-colors"
              >
                Sign In
              </button>
            </div>
          </form>
        </div>
        <div className="bg-gray-200 rounded-md p-4 flex justify-center items-center">
          <img
            src={signin}
            alt="Authentication Visual"
            className="w-full rounded-md"
          />
        </div>
      </div>
    </div>
  );
};

export default AuthForm;
