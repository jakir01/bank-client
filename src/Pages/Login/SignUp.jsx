import axios from "axios";
import { useEffect, useState } from "react";

const SignUp = () => {
  const [formState, setFormState] = useState({
    username: "",
    branchName: "",
    email: "",
    password: "",
    role: "",
    bankId: "",
    bankName: "",
  });
  const [errors, setErrors] = useState({});
  const [banks, setBanks] = useState([]); // State to store the list of banks
  const [branches, setBranches] = useState([]); // State to store the list of branches for selected bank

  useEffect(() => {
    // Fetch the list of banks from the endpoint when the component mounts
    const fetchBanks = async () => {
      try {
        const response = await axios.get("http://localhost:5000/bank");
        setBanks(response.data);
      } catch (error) {
        console.error("Error fetching banks:", error);
        // Handle error
      }
    };

    fetchBanks();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleBankChange = async (e) => {
    const selectedBankId = e.target.value;
    const selectedBank = banks.find((bank) => bank._id === selectedBankId);

    setFormState((prevState) => ({
      ...prevState,
      bankId: selectedBankId ? selectedBank.bankId : "",
      bankName: selectedBank ? selectedBank.bankName : "",
    }));

    // Save the selected bank ID in localStorage
    localStorage.setItem("selectedBankId", selectedBankId);

    // Fetch branches for the selected bank
    if (selectedBankId) {
      try {
        const response = await axios.get(`http://localhost:5000/branches/${selectedBankId}`);
        setBranches(response.data);
      } catch (error) {
        console.error("Error fetching branches:", error);
        // Handle error
      }
    } else {
      setBranches([]);
    }
  };

  // const generateBranchId = () => {
  //   const selectedBankId = localStorage.getItem("selectedBankId");
  //   if (formState.role === "branch" && selectedBankId) {
  //     const bankBranchesCount = parseInt(localStorage.getItem(`${selectedBankId}-branchCount`) || "0", 10);
  //     const newBranchId = (bankBranchesCount + 1).toString().padStart(8, '0');
  //     return newBranchId;
  //   }

  //   return "00000001";
  // };
  const generateBranchId = () => {
    const selectedBankId = localStorage.getItem("selectedBankId");
    if (formState.role === "branch" && selectedBankId) {
      const randomBranchNumber = Math.floor(Math.random() * 10000000); // Generate random number between 0 and 9999999
      const newBranchId = `a${randomBranchNumber.toString().padStart(7, '0')}`; // Pad the random number with zeros to ensure it's 7 digits long
      return newBranchId;
    }
  
    return ""; // Return empty string if conditions are not met
  };
  

  
  const validateForm = () => {
    let tempErrors = {};
    if (formState.role === "user" && !formState.username.trim())
      tempErrors.username = "Username is required";
    if (formState.role === "branch" && !formState.branchName.trim())
      tempErrors.branchName = "Branch name is required";
    if (!formState.email.trim()) tempErrors.email = "Email is required";
    else if (!/\S+@\S+\.\S+/.test(formState.email))
      tempErrors.email = "Email is invalid";
    if (!formState.password) tempErrors.password = "Password is required";
    else if (formState.password.length < 6)
      tempErrors.password = "Password must be at least 6 characters long";
    if (formState.role === "") tempErrors.role = "Role is required";
    if (formState.role === "branch" && !formState.bankId)
      tempErrors.bankId = "Bank is required for branch role";

    setErrors(tempErrors);
    return Object.keys(tempErrors).length === 0;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        let endpoint = "";
        if (formState.role === "user") {
          endpoint = "http://localhost:5000/user/signup";
        } else if (formState.role === "branch") {
          endpoint = "http://localhost:5000/branch/signup";
          formState.branchId = generateBranchId();
        }

        if (endpoint !== "") {
          const response = await fetch(endpoint, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              username: formState.username,
              branchName: formState.branchName,
              email: formState.email,
              password: formState.password,
              role: formState.role,
              bankId: formState.bankId,
              bankName: formState.bankName,
              branchId: formState.branchId,
              status: "active", // Assuming status is always active for now
            }),
          });

          if (!response.ok) {
            throw new Error("Failed to sign up");
          }

          const data = await response.json();
          console.log("Signup successful:", data);

          // Update the branch count in localStorage
          const selectedBankId = localStorage.getItem("selectedBankId");
          const branchCount = parseInt(localStorage.getItem(`${selectedBankId}-branchCount`) || "0", 10);
          localStorage.setItem(`${selectedBankId}-branchCount`, branchCount + 1);

          // Optionally, redirect to a login page or dashboard after successful signup
          window.location.href = "http://localhost:5173/login";
        }
      } catch (error) {
        console.error("Error signing up:", error.message);
        // Handle error (e.g., display error message)
      }
    } else {
      console.log("Form has errors");
    }
  };

  return (
    <div className="min-h-screen flex flex-col items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <h2 className="mt-6 text-center text-3xl font-extrabold bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 bg-clip-text text-transparent">
            Create an Account
          </h2>
        </div>
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>

        <div>
            <select
              name="role"
              value={formState.role}
              onChange={handleChange}
              className={`appearance-none rounded-none relative block w-full px-3 py-2 border ${
                errors.role ? "border-red-500" : "border-gray-300"
              } bg-white text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`}
            >
              <option value="">Select your role</option>
              <option value="user">User</option>
              <option value="branch">Agent</option>
            </select>
            {errors.role && (
              <p className="text-red-500 text-xs italic">{errors.role}</p>
            )}
          </div>
          {formState.role === "branch" && (
            <div>
              <select
                name="bankId"
                value={formState.bankId}
                onChange={handleBankChange}
                className={`appearance-none rounded-none relative block w-full px-3 py-2 border ${
                  errors.bankId ? "border-red-500" : "border-gray-300"
                } bg-white text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`}
              >
                <option value="">Select your bank</option>
                {banks.map((bank) => (
                  <option key={bank._id} value={bank._id}>
                    {bank.bankName}
                  </option>
                ))}
              </select>
              {errors.bankId && (
                <p className="text-red-500 text-xs italic">{errors.bankId}</p>
              )}
            </div>
          )}

          {formState.role === "user" && (
            <div>
              <input
                type="text"
                name="username"
                value={formState.username}
                onChange={handleChange}
                className={`appearance-none rounded-none relative block w-full px-3 py-2 border ${
                  errors.username ? "border-red-500" : "border-gray-300"
                } placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
                placeholder="Username"
              />
              {errors.username && (
                <p className="text-red-500 text-xs italic">
                  {errors.username}
                </p>
              )}
            </div>
          )}

          {formState.role === "branch" && (
            <div>
              <input
                type="text"
                name="branchName"
                value={formState.branchName}
                onChange={handleChange}
                className={`appearance-none rounded-none relative block w-full px-3 py-2 border ${
                  errors.branchName ? "border-red-500" : "border-gray-300"
                } placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
                placeholder="Agent Name"
              />
              {errors.branchName && (
                <p className="text-red-500 text-xs italic">
                  {errors.branchName}
                </p>
              )}
            </div>
          )}

          <div>
            <input
              type="email"
              name="email"
              value={formState.email}
              onChange={handleChange}
              className={`appearance-none rounded-none relative block w-full px-3 py-2 border ${
                errors.email ? "border-red-500" : "border-gray-300"
              } placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
              placeholder="Email address"
            />
            {errors.email && (
              <p className="text-red-500 text-xs italic">{errors.email}</p>
            )}
          </div>

          <div>
            <input
              type="password"
              name="password"
              value={formState.password}
              onChange={handleChange}
              className={`appearance-none rounded-none relative block w-full px-3 py-2 border ${
                errors.password ? "border-red-500" : "border-gray-300"
              } placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
              placeholder="Password"
            />
            {errors.password && (
              <p className="text-red-500 text-xs italic">{errors.password}</p>
            )}
          </div> 
          <div>
            <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-bold rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Sign Up
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
