import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
  const [user, setUser] = useState(null);
  const [userRole, setUserRole] = useState(null);

  useEffect(() => {
    const loggedInUser = localStorage.getItem("user");
    const role = localStorage.getItem("userRole");
    if (loggedInUser && role) {
      setUser(JSON.parse(loggedInUser));
      setUserRole(role);
    }
  }, []);

  const handleLogout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("userRole");
    setUser(null);
    setUserRole(null);
    window.location.reload();
    window.location.href = "/login";
  };

  const isAdmin = userRole === "admin";
  const isBranch = userRole === "branch";
  const isBank = userRole === "bank";
  const isUser = userRole === "user";

  const Nav = (
    <>
      {isUser && !isAdmin && (
        <li>
          <Link to="/home">Home</Link>
        </li>
      )}
      {!user && (
        <li>
          <Link to="/login">Login</Link>
        </li>
      )}
      {user && (
        <>
          {!isAdmin && isBank && (
            <li>
              <Link to="/banks">Insurance Company</Link>
            </li>
          )}
          {!isAdmin && isBranch && (
            <li>
              <Link to="/branch">Branch</Link>
            </li>
          )}
          {isAdmin && (
            <>
              <li>
                <Link to="/bangladeshbank">BD Bank</Link>
              </li>
              <li>
                <Link to="/userverified">Verified User</Link>
              </li>
            </>
          )}
          {user && (
            <li>
              <div className="text-center text-gray-900 font-bold mb-4">
                {isAdmin ? (
                  <span>Admin</span>
                ) : (
                  <>
                    {user.bankName}{" "}
                    <Link to="/userdashboard">{user.username}</Link>
                  </>
                )}
              </div>
            </li>
          )}

          <li>
            <button onClick={handleLogout}>Logout</button>
          </li>
        </>
      )}
    </>
  );

  return (
    <div className="sticky top-0 navbar bg-base-100">
      <div className="navbar-start">
        <div className="dropdown">
          <div tabIndex={0} role="button" className="btn btn-ghost lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16"
              />
            </svg>
          </div>
          <ul
            tabIndex={0}
            className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
          >
            {Nav}
          </ul>
        </div>
      </div>
      <div className="navbar-end hidden lg:flex">
        <ul className="menu menu-horizontal font-semibold px-1">{Nav}</ul>
      </div>
    </div>
  );
};

export default NavBar;
