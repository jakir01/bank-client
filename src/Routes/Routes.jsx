import { createBrowserRouter } from "react-router-dom";
import Main from "../Layout/Main";
import Home from "../Pages/Home/Home";
import BankDashboard from "../Pages/Dashboard/BankDashboard ";
import BangladeshBankDashbard from "../Pages/Dashboard/BangladeshBankDashbard";
import UserVerified from "../Pages/Dashboard/UserVerified";
import Login from "../Pages/Login/Login";
import BanglaDeshBank from "../Pages/Dashboard/BanglaDeshBank";
import Banks from "../Pages/Dashboard/Banks";
import BankDetails from "../Pages/Dashboard/BankDetails";
import Branche from "../Pages/Dashboard/Branche";
import UserDashboard from "../Pages/Dashboard/UserDashboard";
import SignUp from "../Pages/Login/SignUp";
import MyAccounts from "../Pages/Dashboard/MyAccounts";
import Notifications from "../Pages/Dashboard/Notifications";
// import About from "../Pages/About/About";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Main></Main>,
    children: [
      {
        path: "/home",
        element: <Home></Home>,
      },
      // {
      //   path: "/about",
      //   element: <About></About>,
      // },
      {
        path: "/login",
        element: <Login></Login>,
      },
      {
        path: "/signup",
        element: <SignUp></SignUp>
      },
      {
        path: "/bankdashboard",
        element: <BankDashboard></BankDashboard>,
      },
      {
        path: "/userdashboard",
        element: <UserDashboard></UserDashboard>,
      },
      {
        path: "/userdashboard/notifications",
        element: <Notifications></Notifications>,
      },
      {
        path: "/userdashboard/myaccounts",
        element: <MyAccounts></MyAccounts>,
      },
      {
        path: "/banks",
        element: <Banks></Banks>,
      },
      {
        path: "/bankdetails/:branchId", // Note the ":branchId" dynamic parameter
        element: <BankDetails></BankDetails>,
    },
    
      {
        path: "/bangladeshbankdashboard",
        element: <BangladeshBankDashbard></BangladeshBankDashbard>,
      },
      {
        path: "/bangladeshbank",
        element: <BanglaDeshBank></BanglaDeshBank>,
      },
      {
        path: "/branch",
        element: <Branche></Branche>
      },
      {
        path: "/userverified",
        element: <UserVerified></UserVerified>,
      },
    ],
  },
]);
